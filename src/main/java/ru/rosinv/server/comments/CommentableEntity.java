package ru.rosinv.server.comments;

import static ru.rosinv.server.tools.Tools.notNull;

public class CommentableEntity {

    private Boolean has_comments;

    public void setHasComments(Boolean hasComments) {
        this.has_comments = hasComments;
    }

    public Boolean getHasComments() {
        return notNull(has_comments, false);
    }
}
