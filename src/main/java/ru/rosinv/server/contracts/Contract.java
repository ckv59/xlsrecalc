package ru.rosinv.server.contracts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import static java.util.Comparator.naturalOrder;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import ru.rosinv.server.comments.CommentableEntity;
import ru.rosinv.server.contracts.jobtype.JobType;
import ru.rosinv.server.requests.Request;
import static ru.rosinv.server.tools.Tools.empty;
import static ru.rosinv.server.tools.Tools.equal;
import static ru.rosinv.server.tools.Tools.notNull;
import static ru.rosinv.server.tools.Tools.rusDateFormat;

public class Contract extends CommentableEntity{

    private String id;
    private String customer_id;
    private String customer_name;
    private String number;
    private String date;
    private String plan_date;// срок исполнения
    @SuppressWarnings("unused")
	private BigDecimal contract_amount;
    @SuppressWarnings("unused")
	private BigDecimal contract_vat;
    @SuppressWarnings("unused")
	private BigDecimal contract_vat_amount;
    private BigDecimal contract_total_amount;
    private String sign_status;
    private List<ContractJob> jobs;
    private String division_id;
    private String division_name;
    private String client_type_id;
    private String client_type_name;
    private String client_type_name_short;
    private String signatory_id;
    private String signatory_name;
    private String contract_file_id;
    private String contract_file_name;
    private String file_exists;
    private List<ContractAcceptDoc> acceptance;

    private List<ContractPayment> payments;
    private String paid;
    private String accepted;
    private Boolean numberFixed;
    private Boolean withoutPayment;

    private String daytype;// как срок работ в днях считать, calendar/business
    private List<ContractFile> files;

    private String workingCalendarId;
    private String processingStatus;
    private Long updated;
    private Request request;
    private Integer frameContract;
    
    public Integer getFrameContractFlag() {
    	return frameContract;
    }
    
    public Boolean getNumberFixed() {
    	return numberFixed;
    }

    public String getDivisionId() {
        return notNull(division_id, "");
    }

    public String getDate() {
        return notNull(date, "");
    }

    public static final Comparator<Contract> orderDateDesc = (o1, o2) -> {
        if (o1 == null || o1.getDate() == null) {
            return 1;
        } else if (o2 == null || o2.getDate() == null) {
            return -1;
        } else {
            return o2.getDate().compareTo(o1.getDate());
        }
    };

    public String getNumber() {
        return notNull(number, "");
    }

    public String getCustomerName() {
        return notNull(customer_name, "");
    }

    public String getCustomerId() {
        return customer_id;
    }

    public String getDivisionName() {
        return notNull(division_name, "");
    }

    public void setDivisionName(String divisionName) {
        this.division_name = divisionName;
    }

    public String getSignStatus() {
        return sign_status;
    }

    public String getId() {
        return id;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public List<ContractJob> getJobs() {
        if (jobs == null) {
            jobs = new ArrayList<>();
        }
        return jobs;
    }

    public Stream<ContractJob> getJobss() {
        return getJobs().stream();
    }

    public String getJoinJobNames() {
        return String.join(", ", getJobs().stream().map(ContractJob::getJobTypeName).collect(toList()));
    }

    public String getClientTypeId() {
        return client_type_id;
    }

    public String getSignatory_id() {
        return signatory_id;
    }
    
    public String getSignatoryName() {
    	return signatory_name;
    }

    public void setAmount(BigDecimal amount) {
        this.contract_total_amount = amount;
    }

    public BigDecimal getAmount() {
        return contract_total_amount;
    }

    public String getFileId() {
        return contract_file_id;
    }

    public List<ContractPayment> getPayments() {
        if (payments == null) {
            payments = new ArrayList<>();
        }
        return payments;
    }

    public String getPaymentsFirstDate() {
        return getPayments().stream().map(ContractPayment::getDate).filter(s->!s.isEmpty()).min(naturalOrder()).orElse(null);
    }

    public List<ContractAcceptDoc> getAcceptance() {
        if (acceptance == null) {
            acceptance = new ArrayList<>();
        }
        return acceptance;
    }
    
    public Stream<ContractAcceptDoc> getAcceptances() {
        if (acceptance == null) {
            acceptance = new ArrayList<>();
        }
        return getAcceptance().stream();
    }
    
    public Stream<String> getAccountingPeriods(){
    	return getAcceptances().filter(d->!empty(d.getAccountingPeriod())).map(ContractAcceptDoc::getAccountingPeriod);
    }
    
    public String getClientTypeNameShort() {
        return client_type_name_short;
    }

    public String getClientTypeName() {
        return client_type_name;
    }

    public void setClientTypeNameShort(String clientTypeNameShort) {
        this.client_type_name_short = clientTypeNameShort;
    }

    public String getPlanDate() {
        if (plan_date == null) {
            return "";
        } else if (plan_date.length() > 10) {
            return plan_date.substring(0, 10);
        } else {
            return plan_date;
        }
    }

    public void setPlanDate(String planDate) {
        this.plan_date = planDate;
    }

    public String getFileExists() {
        return file_exists;
    }

    public void setFileExists(String fileExists) {
        this.file_exists = fileExists;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getAccepted() {
        return accepted;
    }

    public void setAccepted(String accepted) {
        this.accepted = accepted;
    }

    public Boolean getWithoutPayment() {
        return notNull(withoutPayment, false);
    }

    public String getDaytype() {
        return daytype;
    }

    public void setDaytype(String daytype) {
        this.daytype = daytype;
    }

    public List<ContractFile> getFiles() {
        if (files == null) {
            files = new ArrayList<>();
        }
        return files;
    }

    public String getFileName() {
        return contract_file_name;
    }

    public String getWorkingCalendarId() {
        return workingCalendarId;
    }

    public void setWorkingCalendarId(String workingCalendarId) {
        this.workingCalendarId = workingCalendarId;
    }

    public Stream<ContractJobPart> getJobParts(String jobTypeId) {
        return getJobs().stream().filter(job -> Objects.equals(job.getJobTypeId(), jobTypeId)).flatMap(job -> job.getParts().stream());
    }
    
    public Stream<ContractJobPart> getJobParts(UUID jobId) {
        return getJobs().stream().filter(job -> job.getId() != null && job.getId().equals(jobId)).flatMap(job -> job.getParts().stream());
    }
    
    public Stream<String> getDivisions() {
        Set<String> result = new HashSet<>();
        result.add(division_id);
        getJobs().forEach(job -> job.getParts().forEach(part -> result.add(part.getDivisionId())));
        return result.stream();
    }

    public String getDivisionsString() {
        return String.join(",", getDivisions().collect(toList()));
    }

    public String getProcessingStatus() {
        return processingStatus;
    }

    public void setProcessingStatus(String processingStatus) {
        this.processingStatus = processingStatus;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public String getAccountingStatus() {
        return getAcceptance().stream().anyMatch(cad -> equal(cad.getAccountingStatus(), "+")) ? "+" : "-";
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCustomerId(String customerId) {
        this.customer_id = customerId;
    }

    public void setCustomerName(String customer_name) {
        this.customer_name = customer_name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void addJob(JobType jobType, BigDecimal amount, Integer durationWorkDays) {
        ContractJob contractJob = new ContractJob();
        contractJob.setJobTypeId(jobType.getId());
        contractJob.setJobTypeName(jobType.getName());
        contractJob.setAmount(amount);
        contractJob.setPlanDays(durationWorkDays);
        if (jobs == null) {
            jobs = new ArrayList<>();
            jobs.add(contractJob);
        }
    }

    public void setDivisionId(String division_id) {
        this.division_id = division_id;
    }

    public void setClientTypeId(String client_type_id) {
        this.client_type_id = client_type_id;
    }

    public void setClientTypeName(String client_type_name) {
        this.client_type_name = client_type_name;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public void setSignStatus(String sign_status) {
        this.sign_status = sign_status;
    }

    public String getRequestId() {
        return getRequest() == null ? null : getRequest().getId();
    }
    
    public String getObjectAddress(){
        return getRequest()==null?"":getRequest().getObjectAddress();
    }

    public String getNumberDate() {
        List<String> l = new ArrayList<>();
        if (!empty(getNumber())) {
            l.add(getNumber());
        }
        if (!empty(getDate())) {
            l.add(rusDateFormat(getDate()));
        }
        return String.join(", ", l);
    }

    public Request getRequest() {
        return request;
    }

    public Stream<String> getDoers() {
        Set<String> result = new HashSet<>();
        getJobs().forEach(job -> job.getParts().forEach(part -> result.add(part.getDoersString())));
        return result.stream();
    }

    public String getDoersString() {
        return String.join(", ", getDoers().collect(toList()));
    }

}
