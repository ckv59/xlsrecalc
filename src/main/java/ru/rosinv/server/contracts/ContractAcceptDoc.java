package ru.rosinv.server.contracts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import static ru.rosinv.server.tools.Tools.notNull;

public class ContractAcceptDoc {

    private String id;
    private String accept_type;
    private String accept_name;
    private String accept_date;
    private String accept_accounting_period;
    @SuppressWarnings("unused")
	private BigDecimal accept_accounting_amount;
    private String accept_file_id;
    private String accept_file_name;
    private BigDecimal accept_amount;

    private String accept_number;
    private List<ContractJob> jobs;
    private String vat_invoice_number;
    private String vat_invoice_date;

    private String accept_signatory_id;
    private String accept_signatory_name;
    private String vat_invoice_signatory_id;
    private String vat_invoice_signatory_name;

    private String accounting_status;
    private String accounting_comments;
    private String multiPartStatus;
    private String prefix;
    private Boolean accountingReady;
    private String vatInvoiceFileId;
    private Boolean vatInvoiceChecked;

    public String getPrefix() {
    	return prefix;
    }
    
    public String getAcceptFileId() {
        return notNull(accept_file_id, "");
    }

    public String getId() {
        return id;
    }

    public String getNumber() {
        return notNull(accept_number, "");
    }

    public String getDate() {
        String d = notNull(accept_date, "");
        if (d.length() > 10) {
            d = d.substring(0, 10);// вдруг нам кто-то время подсунул
        }
        return d;
    }

    public BigDecimal getAmount() {
        return notNull(accept_amount, BigDecimal.ZERO);
    }

    public void setAmount(BigDecimal amount) {
        this.accept_amount = amount;
    }

    public List<ContractJob> getJobs() {
        if(jobs==null){
            jobs=new ArrayList<>();
        }
        return jobs;
    }

    public Stream<ContractJob> getJobss() {
        return jobs.stream();
    }
    
    public String getAcceptSignatoryID() {
        return accept_signatory_id;
    }
    
    public String getAcceptSignatoryName() {
    	return accept_signatory_name;
    }

    public String getVatInvoiceSignatoryID() {
        return vat_invoice_signatory_id;
    }
    
    public String getVatInvoiceSignatoryName() {
    	return vat_invoice_signatory_name;
    }

    public String getVatInvoiceNumber() {
        return vat_invoice_number;
    }

    public String getVatInvoiceDate() {
        return vat_invoice_date;
    }

    public String getType() {
        return notNull(accept_type, "");
    }

    public String getName() {
        return accept_name;
    }

    public String getAccountingStatus() {
        return notNull(accounting_status, "-");
    }

    /**
     * Учетный период бухгалтерии
     */
    public String getAccountingPeriod() {
        return accept_accounting_period;
    }

    public void setAccountingPeriod(String period) {
        this.accept_accounting_period = period;
    }

    public void setAccountingStatus(String accounting_status) {
        this.accounting_status = accounting_status;
    }

    public String getAccountingComments() {
        return accounting_comments;
    }

    public void setAccountingComments(String comments) {
        this.accounting_comments = comments;
    }

    public void setMultiPartStatus(String multiPartStatus) {
        this.multiPartStatus = multiPartStatus;
    }

    public Boolean getAccountingReady() {
        return notNull(accountingReady, false);
    }

    public void setAccountingReady(Boolean accountingReady) {
        this.accountingReady = accountingReady;
    }

    public String getVatInvoiceFileId() {
        return vatInvoiceFileId;
    }

    public String getAcceptFileName() {
        return accept_file_name;
    }

    public String getMultiPartStatus() {
        return multiPartStatus;
    }

    public boolean getVatInvoiceChecked() {
        return notNull(vatInvoiceChecked, false);
    }

    public void setVatInvoiceChecked(boolean checked) {
        vatInvoiceChecked = checked;
    }
}
