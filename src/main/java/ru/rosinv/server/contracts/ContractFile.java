package ru.rosinv.server.contracts;

public class ContractFile {

    private String id;
    private String name;
    private String doc_type_id;
    private String doc_type_name;

    public ContractFile() {

    }

    public ContractFile(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public ContractFile(String id, String docTypeName, String name) {
        this.id = id;
        this.name = name;
        this.doc_type_name = docTypeName;
    }

    public String getId() {
        return id;
    }
    
    public String getDocTypeId() {
    	return doc_type_id;
    }
    
    public String getDocTypeName() {
    	return doc_type_name;
    }
    
    public String getName() {
    	return name;
    }
}
