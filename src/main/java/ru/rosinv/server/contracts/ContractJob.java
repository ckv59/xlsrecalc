package ru.rosinv.server.contracts;

import java.math.BigDecimal;
import static java.math.BigDecimal.ZERO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import static ru.rosinv.server.tools.Tools.notNull;

public class ContractJob {

    private String job_type_id;
    private String job_type_name;
    private BigDecimal job_amount;
    @SuppressWarnings("unused")
	private BigDecimal job_vat;
    @SuppressWarnings("unused")
	private BigDecimal job_vat_amount;
    private BigDecimal job_total_amount;
    private String job_plan_date;
    private List<ContractJobPart> parts;
    private Integer planDays;
    private UUID job_id;
    private String act_id;

    public String getJobTypeId() {
        return job_type_id;
    }

    public BigDecimal getAmount() {
        // косяк
        // в договоре сохраняется в job_total_amount
        // в акте сохраняется в job_amount
        return notNull(job_total_amount, ZERO).max(notNull(job_amount, ZERO));
    }

    public void setAmount(BigDecimal amount) {
        job_amount = amount;
        job_total_amount = amount;
    }

    public String getPlanDate() {
        return notNull(job_plan_date, "");
    }

    public List<ContractJobPart> getParts() {
        if (parts == null) {
            parts = new ArrayList<>();
        }
        return parts;
    }

    public Integer getPlanDays() {
        return planDays;
    }

    public String getJobTypeName() {
        return notNull(job_type_name, "");
    }

    public void setPlanDate(String date) {
        this.job_plan_date = date;
    }

    public void setJobTypeId(String job_type_id) {
        this.job_type_id = job_type_id;
    }

    public void setJobTypeName(String job_type_name) {
        this.job_type_name = job_type_name;
    }

    public void setPlanDays(Integer planDays) {
        this.planDays = planDays;
    }

    public UUID getId() {
        return job_id;
    }

    public void setId(UUID id) {
        this.job_id = id;
    }

    public String getActId() {
        return act_id;
    }

    public void setActId(String act_id) {
        this.act_id = act_id;
    }
}
