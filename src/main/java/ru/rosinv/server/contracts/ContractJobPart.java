package ru.rosinv.server.contracts;

import java.math.BigDecimal;
import static java.math.BigDecimal.ONE;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import ru.rosinv.server.tools.Tools;

import static ru.rosinv.server.tools.Tools.notNull;

public class ContractJobPart {

    // распределение работы по подразделениям
    private String id;
    private String divisionId;
    private String divisionName;
    private BigDecimal percent;
    private BigDecimal amount;
    private Boolean percentFixed; 
    // percentFixed: true - пользователи ввели процент, сумму надо считать от суммы работы,
    // false - сумма введена вручную
    // по умолчанию true
    @SuppressWarnings("unused")
	private Long edited; 
    private String fileId;
    private List<ContractJobPartDoer> doers;
    private BigDecimal quality_factor_a;// коэффициент качества работы руководителя, применяется для расчета зп

    public ContractJobPart() {
        // reflection json
    }

    public ContractJobPart(String id, String divisionId, String divisionName, BigDecimal percent) {
        this.id = id;
        this.divisionId = divisionId;
        this.divisionName = divisionName;
        this.percent = percent;
    }
    
    public BigDecimal getAmount() {
    	return amount;
    }
    
    public void setAmount(BigDecimal amount) {
    	this.amount=amount;
    }
    
    public Boolean getPercentFixed() {
    	if(percentFixed==null) {
    		percentFixed=true;
    	}
    	return percentFixed;
    }
    
    public String getId() {
    	return id;
    }

    public String getDivisionId() {
        return notNull(divisionId,"");
    }

    public BigDecimal getPercent() {
        return percent == null ? BigDecimal.ZERO : percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getFileId() {
        return fileId;
    }

    public Stream<ContractJobPartDoer> getDoers() {
        return Tools.notNull(doers, new ArrayList<ContractJobPartDoer>()).stream();
    }

    public String getDoersString() {
        Set<String> result = new HashSet<>();
        getDoers().forEach(doer -> result.add(doer.getName()));

        return String.join(", ", result);
    }

    public BigDecimal getQualityFactorA() {
        return notNull(quality_factor_a, ONE);
    }

    public void setQualityFactorA(BigDecimal qualityFactorA) {
        this.quality_factor_a = qualityFactorA;
    }
}
