package ru.rosinv.server.contracts;

import java.math.BigDecimal;
import java.util.UUID;

public class ContractJobPartDoer {

    private String id;// User.id
    private String name;//User.full_name
    private BigDecimal percent;
    private String personnel_number;// табельный номер
    private BigDecimal quality_factor_b;// коээфициент качества работы исполнителя, используется в расчете зп
    private UUID division_id;// User.division_id
    private String division_name; // User.division_name

    public UUID getDivisionId() {
    	return division_id;
    }
    
    public String getDivisionName() {
    	return division_name;
    }
    /**
    ru.rosinv.server.users.User.id
     */
    public String getId() {
        return id != null ? id : "";
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name != null ? name : "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }

    public String getPersonnelNumber() {
        return personnel_number != null ? personnel_number : "";
    }

    public void setPersonnelNumber(String personnelNumber) {
        this.personnel_number = personnelNumber;
    }

    public BigDecimal getQualityFactorB() {
        return quality_factor_b != null ? quality_factor_b : BigDecimal.ONE;
    }

    public void setQualityFactorB(BigDecimal qualityFactorB) {
        this.quality_factor_b = qualityFactorB;
    }
}
