package ru.rosinv.server.contracts;

import java.math.BigDecimal;
import static ru.rosinv.server.tools.Tools.notNull;

public class ContractPayment {

    private String date;
    private BigDecimal amount;
    private String payment_file_id;
    private String payment_file_name;

    public String getPaymentFileID() {
        return payment_file_id;
    }

    public BigDecimal getAmount() {
        return notNull(amount, BigDecimal.ZERO);
    }

    public String getDate() {
        return notNull(date, "");
    }

    public String getPaymentFileName() {
        return payment_file_name;
    }
}
