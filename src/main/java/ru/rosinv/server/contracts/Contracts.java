package ru.rosinv.server.contracts;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public interface Contracts {
    public CompletableFuture<Stream<Contract>> select();
}
