package ru.rosinv.server.contracts;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import static org.apache.poi.ss.usermodel.BorderStyle.THIN;
import static org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER;
import static org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT;
import ru.rosinv.server.security.File;
import static ru.rosinv.server.tools.Tools.empty;
import static ru.rosinv.server.tools.Tools.rusDateFormat;
import static ru.rosinv.server.tools.Xls.bodyDateStyle;
import static ru.rosinv.server.tools.Xls.bodyNumericStyle;
import static ru.rosinv.server.tools.Xls.bodyStyle;
import static ru.rosinv.server.tools.Xls.createCell;
import static ru.rosinv.server.tools.Xls.headerStyle0;
import static ru.rosinv.server.tools.Xls.headerStyle2;
import static ru.rosinv.server.tools.Xls.textStyle;

public class ContractsXls {

    private final AtomicLong numerator = new AtomicLong();

    public File createXls(List<Contract> listResult,
            String periodFrom,
            String periodTo,
            String filterPlanDateFrom,
            String filterPlanDateTo,
            String filter_number,
            String filter_customer_name,
            String filter_division_name,
            String filter_sign_status,
            String filter_client_type_name_short,
            String filterFileExists,
            String filterPaid,
            String filterAccepted,
            String filterJobs,
            String filterRequestId) {
        try {
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFCellStyle h0 = headerStyle0(wb);
            HSSFCellStyle h3 = headerStyle2(wb, CENTER, THIN);
            HSSFCellStyle bs = bodyStyle(wb);
            HSSFCellStyle bsc = bodyStyle(wb, CENTER);
            HSSFCellStyle st = textStyle(wb, LEFT, 11);// для подписей внизу
            HSSFCellStyle bsn = bodyNumericStyle(wb);
            HSSFCellStyle ds = bodyDateStyle(wb, CENTER);

            HSSFSheet sheet = wb.createSheet("Договоры");
            sheet.getPrintSetup().setLandscape(true);
            sheet.getPrintSetup().setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
            sheet.setColumnWidth(0, 8000);
            sheet.setColumnWidth(1, 4000);
            sheet.setColumnWidth(2, 4300);
            sheet.setColumnWidth(3, 4300);
            sheet.setColumnWidth(4, 4300);
            sheet.setColumnWidth(5, 4300);
            sheet.setColumnWidth(6, 6000);
            sheet.setColumnWidth(7, 6000);
            sheet.setColumnWidth(8, 4300);
            sheet.setColumnWidth(9, 4300);
            sheet.setColumnWidth(10, 4300);
            sheet.setColumnWidth(11, 4300);
            sheet.setColumnWidth(12, 4300);
            sheet.setColumnWidth(13, 4300);
            sheet.setColumnWidth(14, 4300);
            sheet.setColumnWidth(15, 9000);

            // шапка
            int r = 0;
            createCell(sheet, ++r, 0, "Договоры", h0, 1, 16);
            r++;
            createCell(sheet, ++r, 0, "Фильтры:", st);
            if (!empty(periodFrom)) {
                createCell(sheet, ++r, 0, "Период с", st);
                createCell(sheet, r, 1, rusDateFormat(periodFrom), st);
            }
            if (!empty(periodTo)) {
                createCell(sheet, ++r, 0, "Период по", st);
                createCell(sheet, r, 1, rusDateFormat(periodTo), st);
            }
            if (!empty(filterPlanDateFrom)) {
                createCell(sheet, ++r, 0, "Срок с", st);
                createCell(sheet, r, 1, rusDateFormat(filterPlanDateFrom), st);
            }
            if (!empty(filterPlanDateTo)) {
                createCell(sheet, ++r, 0, "Срок по", st);
                createCell(sheet, r, 1, rusDateFormat(filterPlanDateTo), st);
            }
            if (!empty(filter_number)) {
                createCell(sheet, ++r, 0, "Номер", st);
                createCell(sheet, r, 1, filter_number, st);
            }
            if (!empty(filter_customer_name)) {
                createCell(sheet, ++r, 0, "Заказчик", st);
                createCell(sheet, r, 1, filter_customer_name, st);
            }
            if (!empty(filter_division_name)) {
                createCell(sheet, ++r, 0, "Подразделение", st);
                createCell(sheet, r, 1, filter_division_name, st);
            }
            if (!empty(filter_sign_status)) {
                createCell(sheet, ++r, 0, "Статус", st);
                createCell(sheet, r, 1, filter_sign_status, st);
            }
            if (!empty(filter_client_type_name_short)) {
                createCell(sheet, ++r, 0, "Тип заказчика", st);
                createCell(sheet, r, 1, filter_client_type_name_short, st);
            }
            if (!empty(filterFileExists)) {
                createCell(sheet, ++r, 0, "Скан", st);
                createCell(sheet, r, 1, filterFileExists, st);
            }
            if (!empty(filterPaid)) {
                createCell(sheet, ++r, 0, "Оплата", st);
                createCell(sheet, r, 1, filterPaid, st);
            }
            if (!empty(filterAccepted)) {
                createCell(sheet, ++r, 0, "Акт", st);
                createCell(sheet, r, 1, filterAccepted, st);
            }
            if (!empty(filterJobs)) {
                createCell(sheet, ++r, 0, "Наименование работ", st);
                createCell(sheet, r, 1, filterJobs, st);
            }
            r++;
            int c = 0;
            // шапка
            createCell(sheet, ++r, c, "Заказчик", h3);
            createCell(sheet, r, ++c, "Подразделение", h3);
            createCell(sheet, r, ++c, "Тип заказчика", h3);
            createCell(sheet, r, ++c, "Номер", h3);
            createCell(sheet, r, ++c, "Наименование работ", h3);
            createCell(sheet, r, ++c, "Сумма", h3);
            createCell(sheet, r, ++c, "Статус", h3);
            createCell(sheet, r, ++c, "Исполнители", h3);
            createCell(sheet, r, ++c, "Дата", h3);
            createCell(sheet, r, ++c, "Срок", h3);
            createCell(sheet, r, ++c, "Скан", h3);
            createCell(sheet, r, ++c, "Оплата", h3);
            createCell(sheet, r, ++c, "Акт", h3);

            for (int i = 0; i < listResult.size(); i++) {
                Contract contract = listResult.get(i);
                c = 0;
                createCell(sheet, ++r, c, contract.getCustomerName(), bs);
                createCell(sheet, r, ++c, contract.getDivisionName(), bs);
                createCell(sheet, r, ++c, contract.getClientTypeNameShort(), bsc);
                createCell(sheet, r, ++c, contract.getNumber(), bsc);
                createCell(sheet, r, ++c, contract.getJoinJobNames(), bs);
                createCell(sheet, r, ++c, contract.getAmount(), bsn);
                createCell(sheet, r, ++c, contract.getSignStatus(), bs);
                createCell(sheet, r, ++c, contract.getDoersString(), bs);
                createCell(sheet, r, ++c, contract.getDate(), ds);
                createCell(sheet, r, ++c, contract.getPlanDate(), ds);
                createCell(sheet, r, ++c, contract.getFileExists(), bsc);
                createCell(sheet, r, ++c, contract.getPaid(), bsc);
                createCell(sheet, r, ++c, contract.getAccepted(), bsc);
            }
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            wb.write(bos);
            long n = numerator.incrementAndGet();
            while (n < 0) {
                numerator.set(0);
                n = numerator.incrementAndGet();
            }
            File f = new File("contracts-" + n + ".xls", bos.toByteArray(), "application/vnd.ms-excel");
            return f;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
