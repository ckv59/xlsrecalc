package ru.rosinv.server.contracts.jobtype;

import ru.rosinv.server.db.DbObject;

public class JobType implements DbObject{

    private String id;
    private String parent_id;
    private String code;
    private String name;
    private String status;
    private String status_name;

    public String getCode() {
    	return code;
    }
    
    public String getName() {
        return name;
    }

    public String getParentId() {
        return parent_id;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }
    
    public String getStatusName() {
    	return status_name;
    }
}
