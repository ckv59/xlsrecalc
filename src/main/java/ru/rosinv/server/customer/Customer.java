package ru.rosinv.server.customer;

import ru.rosinv.server.db.DbObject;
import ru.rosinv.server.tools.Tools;

public class Customer implements DbObject {

	private String id;
	private String name;
	private String division_id;
	private String division_name;
	private String status;
	private String status_name;
	private String address;
	private String boss_name;
	private String boss_position;
	private String email;
	private String phone;
	private String scope_id;
	private String scope_name;
	private String tin;
	private String comments;
	private String form_id;
	private String form_name;
	private String liquidation_date;
	private String full_name;// form_name + name
	private String date_control;
	// банковские реквизиты
	private String oper_account; // р.с.
	private String bank_name;
	private String bank_code; // БИК
	private String corr_account; // к.с.

	private String psrn; // ОГРН

	public String getId() {
		return id;
	}

	public String getName() {
		return name == null ? "" : name;
	}

	public String getDivision_id() {
		return division_id;
	}

	public String getDivision_name() {
		return division_name;
	}

	public String getStatus() {
		return status;
	}

	public String getStatus_name() {
		return status_name;
	}

	public String getAddress() {
		return address;
	}

	/** Полное: Сивцов Андрей Николаевич */
	public String getBossName() {
		return boss_name;
	}

	/** Сокращенное: Сивцов А. Н. */
	public String getBossNameShort() {
		if (Tools.empty(boss_name)) {
			return "";
		} else {
			String[] split = boss_name.trim().replace("  ", " ").split(" ");
			String result = "";
			for (int i = 0; i < split.length; i++) {
				if (i == 0) {
					result += split[0];
				} else {
					result += " " + split[i].substring(0, 1).toUpperCase() + ".";
				}
			}
			return result;
		}
	}

	public String getBossPosition() {
		return boss_position;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public String getScope_id() {
		return scope_id;
	}

	public String getScope_name() {
		return scope_name;
	}

	/** ИНН/КПП */
	public String getTinFull() {
		return tin;
	}

	/** ИНН */
	public String getTin1() {
		if (Tools.empty(tin)) {
			return "";
		} else {
			String[] s = tin.split("/");
			if (s.length > 0) {
				return s[0];
			} else {
				return "";
			}
		}
	}

	/** КПП */
	public String getTin2() {
		if (Tools.empty(tin)) {
			return "";
		} else {
			String[] s = tin.split("/");
			if (s.length > 1) {
				return s[1];
			} else {
				return "";
			}
		}
	}

	public String getComments() {
		return comments;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDivision_id(String division_id) {
		this.division_id = division_id;
	}

	public void setDivision_name(String division_name) {
		this.division_name = division_name;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setBoss_name(String boss_name) {
		this.boss_name = boss_name;
	}

	public void setBoss_position(String boss_position) {
		this.boss_position = boss_position;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setScope_id(String scope_id) {
		this.scope_id = scope_id;
	}

	public void setScope_name(String scope_name) {
		this.scope_name = scope_name;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getForm_id() {
		return form_id;
	}

	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}

	public String getFormName() {
		return form_name == null ? "" : form_name;
	}

	public void setForm_name(String form_name) {
		this.form_name = form_name;
	}

	public String getLiquidation_date() {
		return liquidation_date;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getDate_control() {
		return date_control;
	}

	public void setDate_control(String date_control) {
		this.date_control = date_control;
	}

	public String getOperAccount() {
		return oper_account;
	}

	public String getBankName() {
		return bank_name;
	}

	public String getBankCode() {
		return bank_code;
	}

	public String getCorrAccount() {
		return corr_account;
	}

	public void setPsrn(String psrn) {
		this.psrn = psrn;
	}

	public String getPsrn() {
		return psrn;
	}
}
