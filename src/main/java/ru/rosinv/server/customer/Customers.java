package ru.rosinv.server.customer;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import ru.rosinv.server.security.AuthenticatedUser;
import ru.rosinv.server.tools.UpdateResult;

public interface Customers {
	public static final String CLIENT_TYPE_LEGAL = "45c9c61b-3ea6-47b1-88cf-4b9e7d302251";
	public static final String CLIENT_TYPE_REAL = "e34d7f85-2500-4766-8aa6-8d24e1260c75";

	public CompletableFuture<UpdateResult> update(AuthenticatedUser authUser, Customer customer);
	public CompletableFuture<UpdateResult> update(AuthenticatedUser authUser, RealCustomer customer);

	public CompletableFuture<Stream<Customer>> selectFiltered(AuthenticatedUser user);

	public CompletableFuture<Stream<RealCustomer>> selectReal(AuthenticatedUser user);
}
