package ru.rosinv.server.customer;

import ru.rosinv.server.db.DbObject;

public class RealCustomer implements DbObject {

	private String id;
	private String division_id;
	private String division_name;
	private String name;
	private String phone;

	private String passport_series;
	private String passport_number;
	private String passport_date;
	private String passport_issued;
	private String address;
	private String email;

	public String getId() {
		return id;
	}

	public String getDivision_id() {
		return division_id;
	}

	public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

	public String getDivision_name() {
		return division_name;
	}

	public String getPassportSeries() {
		return passport_series;
	}

	public String getPassportNumber() {
		return passport_number;
	}

	public String getPassportDate() {
		return passport_date;
	}

	public String getPassportIssued() {
		return passport_issued;
	}

	public String getAddress() {
		return address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
