package ru.rosinv.server.division;

public class ComboOption {

    private String id;
    private String value;

    public ComboOption() {
        //reflection json
    }

    public ComboOption(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getId() {
        return id;
    }

}
