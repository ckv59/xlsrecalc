package ru.rosinv.server.division;

import java.math.BigDecimal;

import ru.rosinv.server.db.DbObject;

import static java.math.BigDecimal.ZERO;
import static ru.rosinv.server.tools.Tools.notNull;

public class Division implements DbObject{

    private String id;
    private String name;
    private String status;
    private String status_name;
    private String comment;
    private String email;
    private String location;
    private String curator_id;
    private String curator_name;
    private String groupid;//DivisionsGroup.id, may be "null"
    private BigDecimal salaryratea;// руководитель
    private BigDecimal salaryrateb;// исполнитель

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return status_name;
    }

    public String getComment() {
        return comment;
    }

    public String getEmail() {
        return email;
    }

    public String getLocation() {
        return location;
    }

    public String getCurator_id() {
        return curator_id;
    }

    public String getCurator_name() {
        return curator_name;
    }

    public String getGroupId() {
        return groupid;
    }

    public void setGroupId(String groupid) {
        this.groupid = groupid;
    }

    public BigDecimal getSalaryrateA() {
        return notNull(salaryratea, ZERO);
    }

    public BigDecimal getSalaryrateB() {
        return notNull(salaryrateb, ZERO);
    }

}
