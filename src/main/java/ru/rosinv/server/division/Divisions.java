package ru.rosinv.server.division;

import java.util.Set;
import java.util.concurrent.CompletableFuture;
import ru.rosinv.server.security.AuthenticatedUser;

public interface Divisions {

    public CompletableFuture<Set<String>> grantedDivs(AuthenticatedUser authUser);
}
