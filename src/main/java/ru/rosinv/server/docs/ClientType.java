package ru.rosinv.server.docs;

import ru.rosinv.server.db.DbObject;

public class ClientType implements DbObject{

    private String id;
    private String name;
    private String shortname;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getShortName() {
        return shortname;
    }
}
