package ru.rosinv.server.docs;

import ru.rosinv.server.db.DbObject;

public class DocNumerator implements DbObject {
	private String id;
	private String division_id;
	private String doc_type_id;
	private String period;
	private Integer next_number;
	private String client_type_id;
	private String pattern;
	private String cycle;

	public String getDivisionId() {
		return division_id;
	}

	public String getDocTypeId() {
		return doc_type_id;
	}

	public String getPeriod() {
		return period;
	}

	public Integer getNextNumber() {
		return next_number;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setDivisionId(String divisionId) {
		this.division_id = divisionId;
	}

	public void setDocTypeId(String docTypeId) {
		this.doc_type_id = docTypeId;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public void setNextNumber(Integer nextNumber) {
		this.next_number = nextNumber;
	}
	
	public String getId() {
		return id;
	}
	
	public String getClientTypeId() {
		return client_type_id;
	}
	
	public void setClientTypeId(String clientTypeId) {
		this.client_type_id=clientTypeId;
	}
	
	public String getPattern() {
		return pattern;
	}
	
	public void setPattern(String pattern) {
		this.pattern=pattern;
	}
	
	public void setCycle(String cycle) {
		this.cycle=cycle;
	}
	
	public String getCycle() {
		return cycle;
	}
}
