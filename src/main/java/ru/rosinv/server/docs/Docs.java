package ru.rosinv.server.docs;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import ru.rosinv.server.division.ComboOption;
import ru.rosinv.server.security.AuthenticatedUser;

public interface Docs {
	public CompletableFuture<String> nextNumber(AuthenticatedUser authUser, String divisionId,
			String docTypeId, String date);
	
	public CompletableFuture<DocNumerator> getNumerator(AuthenticatedUser authUser,String divisionId,String docTypeId,String clientTypeId,String period);
	
	public CompletableFuture<Void> setNumerator(AuthenticatedUser authUser,DocNumerator numerator);
	public CompletableFuture<List<ComboOption>> getSignatories(AuthenticatedUser authUser, String division_id,
			String doc_type_id);
}
