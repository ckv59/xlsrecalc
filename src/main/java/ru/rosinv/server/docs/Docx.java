package ru.rosinv.server.docs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Map;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;
import static ru.rosinv.server.docs.Params.BANK_ACCOUNT;
import static ru.rosinv.server.docs.Params.BANK_CODE;
import static ru.rosinv.server.docs.Params.BANK_NAME;
import static ru.rosinv.server.docs.Params.CONTRACT_AMOUNT;
import static ru.rosinv.server.docs.Params.CONTRACT_ID;
import static ru.rosinv.server.docs.Params.CUSTOMER_ADDRESS;
import static ru.rosinv.server.docs.Params.CUSTOMER_NAME;
import static ru.rosinv.server.docs.Params.DATE;
import static ru.rosinv.server.docs.Params.NUMBER;
import static ru.rosinv.server.docs.Params.PAYEE_ACCOUNT;
import static ru.rosinv.server.docs.Params.PAYEE_NAME;
import static ru.rosinv.server.docs.Params.PAYEE_TIN;
import static ru.rosinv.server.docs.Params.QRSBERBANK;
import static ru.rosinv.server.docs.Params.QRSPECIALCODE;
import static ru.rosinv.server.tools.Tools.empty;
import ru.rosinv.server.tools.qrcode.QrCodeGenerator;
import ru.rosinv.server.tools.qrcode.QrSberbankCodeRequisites;
import ru.rosinv.server.tools.qrcode.QrSpecialCodeValues;

public class Docx {

    private static final int qrSberBarcodeWidth = 400;
    private static final int qrSberBarcodeHeight = 400;

    private static final int qrSpecCodeWidth = 100;
    private static final int qrSpecCodeHeight = 100;

    public static byte[] replaceParams(byte[] template, Map<String, String> params) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(template.length * 2);
            try (XWPFDocument doc = new XWPFDocument(new ByteArrayInputStream(template))) {
                for (XWPFParagraph paragraph : doc.getParagraphs()) {
                    replaceParagraphParams(paragraph, params);
                }
                for (XWPFTable table : doc.getTables()) {
                    replaceTableParams(table, params);
                }

                for (XWPFHeader header : doc.getHeaderList()) {
                    replaceHeaderParams(header, params);
                }

                for (XWPFFooter footer : doc.getFooterList()) {
                    replaceFooterParams(footer, params);
                }

                doc.write(baos);
            }
            return baos.toByteArray();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void replaceParagraphParams(XWPFParagraph paragraph, Map<String, String> params) throws Exception {
        if (paragraphText(paragraph).contains(QRSBERBANK)) {
            insertSberQrCode(paragraph, params);
            return;
        }

        if (paragraphText(paragraph).contains(QRSPECIALCODE)) {
            insertSpecialCode(paragraph, params);
            return;
        }

        params.forEach((search, replace) -> {
            while (paragraphText(paragraph).contains(search)) {
                String paragraphText = paragraphText(paragraph);
                int i = paragraphText.indexOf(search);
                int i1 = i + search.length();
                int j = 0;
                for (XWPFRun run : paragraph.getRuns()) {
                    for (CTText ctt : run.getCTR().getTArray()) {
                        String rt = ctt.getStringValue();
                        int j1 = j + rt.length();
                        if (j1 <= i) {
                            // before, skip
                        } else if (j <= i && j1 <= i1) {
                            // тут начало
                            String nw = rt.substring(0, i - j) + replace;
                            ctt.setStringValue(nw);
                        } else if (j > i && j1 <= i1) {
                            // тут часть оставшаяся
                            ctt.setStringValue("");
                        } else if (j <= i && j1 > i1) {
                            // шаблон где-то в середине поместился весь
                            String nw = rt.substring(0, i - j) + replace + rt.substring(i - j + search.length());
                            ctt.setStringValue(nw);
                        } else if (j < i1 && j1 > i1) {
                            // тут тоже часть оставшаяся
                            String nw = rt.substring(search.length() - j + i);
                            ctt.setStringValue(nw);
                        } else if (j >= i1) {
                            // приехали
                            break;
                        }
                        j += rt.length();
                    }
                }
            }
        });

    }

    private static void insertSpecialCode(XWPFParagraph paragraph, Map<String, String> params) throws Exception{
        QrSpecialCodeValues values = new QrSpecialCodeValues();
        values.ContractNumber = params.getOrDefault(NUMBER, "");
        values.ContractDate = params.getOrDefault(DATE, "");
        values.ContractId = params.getOrDefault(CONTRACT_ID, "");

        QrCodeGenerator qrCodeGenerator = new QrCodeGenerator();
            byte[] qrCodeImageData = qrCodeGenerator.getQRCodeImageData(values.getText(), qrSpecCodeWidth, qrSpecCodeHeight);

            removeAllRuns(paragraph);

            XWPFRun run = paragraph.createRun();
            run.addPicture(new ByteArrayInputStream(qrCodeImageData), XWPFDocument.PICTURE_TYPE_PNG, "qrSpecCode.png", Units.toEMU(qrSpecCodeWidth / 2.5), Units.toEMU(qrSpecCodeHeight / 2.5));
    }

    private static void insertSberQrCode(XWPFParagraph paragraph, Map<String, String> params) throws Exception {
        QrSberbankCodeRequisites requisites = new QrSberbankCodeRequisites();
        requisites.Name = params.getOrDefault(PAYEE_NAME, "");
        requisites.PersonalAcc = params.getOrDefault(PAYEE_ACCOUNT, "");
        requisites.BankName = params.getOrDefault(BANK_NAME, "");
        requisites.BIC = params.getOrDefault(BANK_CODE, "");
        requisites.CorrespAcc = params.getOrDefault(BANK_ACCOUNT, "");
        requisites.PayeeINN = params.getOrDefault(PAYEE_TIN, "");
        String customerName = params.getOrDefault(CUSTOMER_NAME, "");
        if (!empty(customerName)) {
            String[] s = customerName.trim().split(" ");
            if (s.length > 0) {
                requisites.LastName = s[0];
            }
            if (s.length > 1) {
                requisites.FirstName = s[1];
            }
            if (s.length > 2) {
                requisites.MiddleName = s[2];
            }
        }
        requisites.PayerAddress = params.getOrDefault(CUSTOMER_ADDRESS, "");
        requisites.Purpose = "№ договора: " + params.getOrDefault(NUMBER, "") + " от " + params.getOrDefault(DATE, "");
        requisites.Sum = params.getOrDefault(CONTRACT_AMOUNT, "").replace(".", "");
        String date = params.getOrDefault(DATE, "");
        if (date.length() >= 10) {
            requisites.paymPeriod = date.substring(3, 10).replace(".", "");
        }

        QrCodeGenerator qrCodeGenerator = new QrCodeGenerator();
        byte[] qrCodeImageData = qrCodeGenerator.getQRCodeImageData(requisites.getText(), qrSberBarcodeWidth, qrSberBarcodeHeight);

        removeAllRuns(paragraph);

        XWPFRun run = paragraph.createRun();
        run.addPicture(new ByteArrayInputStream(qrCodeImageData), XWPFDocument.PICTURE_TYPE_PNG, "qrSberBank.png", Units.toEMU(qrSberBarcodeWidth / 2.5), Units.toEMU(qrSberBarcodeHeight / 2.5));
    }

    private static void removeAllRuns(XWPFParagraph paragraph) {
        int size = paragraph.getRuns().size();
        for (int i = 0; i < size; i++) {
            paragraph.removeRun(0);
        }
    }

    private static void replaceTableParams(XWPFTable table, Map<String, String> params) throws Exception {
        for (XWPFTableRow row : table.getRows()) {
            for (XWPFTableCell cell : row.getTableCells()) {
                for (XWPFParagraph paragraph : cell.getParagraphs()) {
                    replaceParagraphParams(paragraph, params);
                }
                for (XWPFTable subtable : cell.getTables()) {
                    replaceTableParams(subtable, params);
                }
            }
        }
    }

    private static void replaceHeaderParams(XWPFHeader header, Map<String, String> params) throws Exception {
        for (XWPFParagraph paragraph : header.getParagraphs()) {
            replaceParagraphParams(paragraph, params);
        }
    }

    private static void replaceFooterParams(XWPFFooter footer, Map<String, String> params) throws Exception {
        for (XWPFParagraph paragraph : footer.getParagraphs()) {
            replaceParagraphParams(paragraph, params);
        }
    }

    private static String paragraphText(XWPFParagraph paragraph) {
        String paragraphText = "";
        for (XWPFRun run : paragraph.getRuns()) {
            for (CTText ctt : run.getCTR().getTArray()) {
                paragraphText += ctt.getStringValue();
            }
        }
        return paragraphText;
    }
}
