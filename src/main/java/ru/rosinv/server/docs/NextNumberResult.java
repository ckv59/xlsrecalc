package ru.rosinv.server.docs;

public class NextNumberResult {

    private String nextNumber;

    public NextNumberResult() {

    }

    public NextNumberResult(String nextNumber) {
        this.nextNumber = nextNumber;
    }
    
    public String getNextNumber() {
    	return nextNumber;
    }
}
