package ru.rosinv.server.docs;

public class Params {

    // большой QR-код с реквизитами для оплаты
    public static final String QRSBERBANK = "${QRSBERBANK}";
    // маленький QR-код для контроля договоров
    public static final String QRSPECIALCODE = "${QRSPECIALCODE}";
    // Наименование получателя 
    public static final String PAYEE_NAME = "${ПОЛУЧАТЕЛЬ_БАНК}";
    // Расчетный счет получателя 
    public static final String PAYEE_ACCOUNT = "${РСЧ_БАНК}";
    // Наименование банка получателя
    public static final String BANK_NAME = "${НАИМЕНОВАНИЕ_БАНК}";
    // БИК банка получателя
    public static final String BANK_CODE = "${БИК_БАНК}";
    // Корр. счет банка получателя
    public static final String BANK_ACCOUNT = "${КОРСЧ_БАНК}";
    // ИНН получателя платежа
    public static final String PAYEE_TIN = "${ИНН_РОСТЕХ}";
    // Наименование заказчика, для физических лиц ФИО
    public static final String CUSTOMER_NAME = "${ЗАКАЗЧИК_НАИМЕНОВАНИЕ}";
    // Адрес заказчика
    public static final String CUSTOMER_ADDRESS="${ЗАКАЗЧИК_АДРЕС}";
    // Номер документа (договора)
    public static final String NUMBER="${НОМЕР}";
    // Дата документа (договора), 31.07.2018
    public static final String DATE="${ДАТА}";
    // Сумма договора, 10312.50
    public static final String CONTRACT_AMOUNT="${СУММА_ДОГОВОРА}";
    // Идентификатор договора
    public static final String CONTRACT_ID="${ДОГОВОР_ИД}";
}
