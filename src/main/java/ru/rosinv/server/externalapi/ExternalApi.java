package ru.rosinv.server.externalapi;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.rosinv.server.contracts.Contract;
import ru.rosinv.server.contracts.Contracts;
import ru.rosinv.server.customer.Customer;
import ru.rosinv.server.customer.Customers;
import ru.rosinv.server.customer.RealCustomer;
import ru.rosinv.server.division.ComboOption;
import ru.rosinv.server.division.Divisions;
import ru.rosinv.server.docs.DocNumerator;
import ru.rosinv.server.docs.Docs;
import ru.rosinv.server.docs.NextNumberResult;
import ru.rosinv.server.file.Files;
import ru.rosinv.server.requests.Request;
import ru.rosinv.server.requests.Requests;
import ru.rosinv.server.requests.price.CalcParam;
import ru.rosinv.server.requests.price.CalculateRequest;
import ru.rosinv.server.requests.price.RequestsPrice;
import ru.rosinv.server.security.ApiComponent;
import ru.rosinv.server.security.AuthenticatedUser;
import ru.rosinv.server.security.BadRequest;
import ru.rosinv.server.security.File;
import ru.rosinv.server.security.Forbidden;

import static ru.rosinv.server.security.Roles.ADMIN_ROLE_ID;
import static ru.rosinv.server.security.Roles.BOSS_ROLE_ID;
import static ru.rosinv.server.security.Roles.CONSULTANT_ROLE_ID;
import static ru.rosinv.server.security.Roles.CURATOR_ROLE_ID;
import static ru.rosinv.server.security.Roles.DEPUTY_BOSS_ROLE_ID;
import static ru.rosinv.server.security.Roles.OPERATOR_ROLE_ID;

import ru.rosinv.server.tools.*;
import ru.rosinv.xls.recalc.XLSReader;

import static ru.rosinv.server.tools.Tools.empty;
import static ru.rosinv.server.tools.Tools.notNull;

import static ru.rosinv.server.tools.Tools.likeFilter;
import static ru.rosinv.server.tools.Tools.nullFirstCompare;
import static ru.rosinv.server.tools.Tools.compare;
import static ru.rosinv.server.docs.DocTypes.VAT_INVOICE_DOC_TYPE_ID;

/*
синглтон,
апи для внешних систем
 */
public class ExternalApi implements ApiComponent {

	private static final Logger logger = LogManager.getLogger();
	private static final String[] REQUESTS_ROLES = { OPERATOR_ROLE_ID, CURATOR_ROLE_ID, BOSS_ROLE_ID,
			CONSULTANT_ROLE_ID, ADMIN_ROLE_ID, DEPUTY_BOSS_ROLE_ID };
	private final Requests requestsInternal;
	private final Divisions divisions;
	private final Contracts contracts;
	private final Customers customers;
	private final Files files;
	private final XLSReader xlsReader;
	private final Docs docs;

	public ExternalApi(Requests requestsInternal, Divisions divisions, Contracts contracts, Customers customers,
			Files files, XLSReader xlsReader, Docs docs) {
		this.requestsInternal = requestsInternal;
		this.divisions = divisions;
		this.contracts = contracts;
		this.customers = customers;
		this.files = files;
		this.xlsReader = xlsReader;
		this.docs = docs;
	}

	@Override
	public void start() {
	}

	@Override
	public void stop() {
	}

	@Override
	public Map<String, HttpServerFunction<? extends Object>> getRoutes() {
		Map<String, HttpServerFunction<? extends Object>> httpMap = new HashMap<>();
		httpMap.put("GET/external/requests", new HttpAuthFunction<>(WebixGridQuery.class, u -> get(u)));
		httpMap.put("POST/external/requests", new HttpPostFunction<>(Request.class, (u, r) -> update(u, r)));
		httpMap.put("GET/external/requests/prices/services",
				new HttpAuthFunction<>(WebixGridQuery.class, u -> getRequestsPricesServices(u)));
		httpMap.put("POST/external/requests/download_calc",
				new HttpPostFunction<>(CalculateRequest.class, (u, d) -> postRequestsDownloadCalc(u, d)));

		httpMap.put("GET/external/log", new HttpGetFunction(authUser -> getLog(authUser)));

		httpMap.put("GET/external/contracts", new HttpGetFunction((u, p) -> listContracts(u, p.get("period_from"),
				p.get("period_to"), p.get("filter[number]"), p.get("filter[division_name]"),
				p.get("filter[customer_name]"), p.get("filter[sign_status]"), p.get("sort[date]"),
				p.get("sort[number]"), p.get("sort[customer_name]"), p.get("sort[division_name]"),
				p.get("sort[sign_status]"), p.get("filter[client_type_name_short]"),
				p.get("sort[client_type_name_short"), p.get("sort[plan_date]"), p.get("filter[file_exists]"),
				p.get("sort[file_exists]"), p.get("filter[paid]"), p.get("sort[paid]"), p.get("filter[accepted]"),
				p.get("sort[accepted]"), p.get("filter[jobs]"), p.get("sort[jobs]"), p.get("processingStatus"),
				p.get("plan_date_from"), p.get("plan_date_to"), p.get("filter[requestId]"), p.get("output"),
				p.get("filter[customer_id]"), p.get("filter[has_comments]"), p.get("filter[division_id]"),
				p.get("filter[without_payment]"), p.get("payments_first_date_from"), p.get("payments_first_date_to"),
				p.get("start"), p.get("count"), p.get("filter[accept_accounting_period_from]"),
				p.get("filter[accept_accounting_period_to]"), p.get("filter[creator_div_id]"))));
		httpMap.put("GET/external/contracts/nextVatInvoiceNumber",
				new HttpGetFunction((u, p) -> nextVatInvoiceNumber(u, p.get("divisionId"), p.get("date"))));

		httpMap.put("POST/external/customer/legal",new HttpPostFunction<>(Customer.class, (u, r) -> postCustomerLegal(u, r)));
		httpMap.put("POST/external/customer/real",new HttpPostFunction<>(RealCustomer.class, (u, r) -> postCustomerReal(u, r)));
		httpMap.put("GET/external/customers/combo",
				new HttpAuthFunction<>(WebixGridQuery.class, u -> getCustomersCombo(u)));

		httpMap.put("GET/external/admin/numerator", new HttpGetFunction((u,p)->getNumerator(u, p.get("division_id"), p.get("doc_type_id"),p.get("client_type_id"), p.get("period"))));
		httpMap.put("POST/external/admin/numerator",new HttpPostFunction<>(DocNumerator.class, (u, r) -> setNumerator(u, r)));
		
		httpMap.put("GET/external/signatories/combo",new HttpGetFunction((u, p) -> getSignatories(u, p.get("division_id"), p.get("doc_type_id"))));

		return httpMap;
	}

	private boolean grantedUser(AuthenticatedUser authUser) {
		return authUser.isExternalTrustedSystem() || authUser.hasAnyRole(REQUESTS_ROLES);
	}

	private CompletableFuture<List<Request>> get(WebixGridQuery q) {
		AuthenticatedUser authUser = q.getAuthUser();
		if (grantedUser(authUser)) {
			return divisions.grantedDivs(authUser).thenCompose(divs -> {
				return requestsInternal.getAllActive().thenApply(sr -> sr.filter(userFilter(divs))
						.filter(likeFilter(q.getFilter("division_id"), Request::getDivisionId))
						.filter(likeFilter(q.getFilter("id"), Request::getId))
						.filter(likeFilter(q.getFilter("request_status"), Request::getRequestStatus))
						.filter(likeFilter(q.getFilter("creator_div_id"), Request::getCreatorDivisionId))
						.filter(request -> empty(q.getFilter("date_from"))
								|| (request.getDate().compareTo(q.getFilter("date_from")) >= 0))
						.filter(request -> empty(q.getFilter("date_to"))
								|| (request.getDate().compareTo(q.getFilter("date_to")) <= 0))
						.filter(Tools.likeFilter(q.getFilter("customer"), Request::getCustomerName))
						.filter(Tools.likeFilter(q.getFilter("customer_id"), Request::getCustomerId))
						.filter(Tools.likeFilter(q.getFilter("division"), Request::getDivisionName))
						.filter(Tools.likeFilter(q.getFilter("number"), r -> r.getNumber().toString()))
						.filter(likeFilter(q.getFilter("serviceNames"), Request::getServiceNames))
						.filter(likeFilter(q.getFilter("amount"), Request::getAmountStr))
						.filter(Tools.likeFilter(q.getFilter("requestStatus"), Request::getRequestStatus))
						.filter(likeFilter(q.getFilter("contractNumbers"), Request::getContractNumbers))
						.sorted(Tools.join(compare(Request::getDate, q.getSort("date")), compare(Request::getId, "asc")
				// дефолтная сортировка обязательно должна быть, sorted не любит плохих
				// компараторов
				)).skip(Long.parseLong(Tools.notEmpty(q.getStart(), "0")))
						.limit(Long.parseLong(Tools.notEmpty(q.getCount(), "50"))).collect(Collectors.toList()));
			});
		} else {
			throw new Forbidden();
		}
	}

	private CompletableFuture<UpdateResult> update(AuthenticatedUser authUser, Request r) {
		// requestsInternal.update проверит доступы
		return requestsInternal.update(authUser, r).thenApply(v -> UpdateResult.SUCCESS_RESULT);
	}

	private CompletableFuture<UpdateResult> postCustomerLegal(AuthenticatedUser authUser, Customer customer) {
		// customers.update проверяет уникальность по инн и доступы, пользователь может
		// менять клиентов только своего подразделения
		return customers.update(authUser, customer);
	}

	private CompletableFuture<UpdateResult> postCustomerReal(AuthenticatedUser authUser, RealCustomer customer) {
		// customers.update проверяет уникальность фио+тел, пользователь может
		// менять клиентов только своего подразделения
		return customers.update(authUser, customer);
	}
	
	private Predicate<Request> userFilter(Set<String> grantedDivs) {
		return request -> grantedDivs.contains(request.getDivisionId());
	}

	private String getLog(AuthenticatedUser authUser) {
		if (authUser.isAdmin() || authUser.isTestAdmin()) {
			String result = null;

			Path logPath = Paths.get("data/log/ru.rosinv.server.log");
			ByteBuffer buffer = ByteBuffer.allocate(1048576);

			try (FileChannel channel = FileChannel.open(logPath, StandardOpenOption.READ)) {
				long position = 0;

				if (channel.size() > 1048576) {
					position = channel.size() - 1048576;
				}

				channel.read(buffer, position);
				result = new String(buffer.array(), StandardCharsets.UTF_8);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

			return result;
		} else {
			throw new Forbidden();
		}
	}

	private Predicate<Contract> contractUserFilter(Set<String> grantedDivs) {
		return contract -> contract.getDivisions().anyMatch(divid -> (grantedDivs.contains(divid)));
	}

	private boolean between(String value, String from, String before) {
		if (empty(from) && empty(before)) {
			return true; // фильтровать не нужно
		} else if (empty(value)) {
			return false; // фильтровать нужно, но значения нет
		} else if (!empty(from) && value.compareTo(from) < 0) {
			return false;
		} else if (!empty(before) && value.compareTo(before) >= 0) {
			return false;
		} else {
			return true;
		}
	}

	private boolean likeFilters(String filterValue, String dataValue) {
		if (filterValue == null || filterValue.isEmpty()) {
			return true;
		} else {
			if (dataValue == null) {
				return false;
			} else {
				return dataValue.toLowerCase().contains(filterValue.toLowerCase());
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Comparator<Contract> buildComparator(String sort_date, String sort_number, String sort_customer_name,
			String sort_division_name, String sort_sign_status, String sort_client_type_name_short, String sortPlanDate,
			String sortFileExists, String sortPaid, String sortAccepted, String sortJobs) {
		@SuppressWarnings("rawtypes")
		Function<Contract, Comparable> sortValue;
		boolean asc;
		if (sort_date != null && !sort_date.isEmpty()) {
			sortValue = a -> a.getDate();
			asc = sort_date.equals("asc");
		} else if (sort_number != null && !sort_number.isEmpty()) {
			sortValue = a -> a.getNumber();
			asc = sort_number.equals("asc");
		} else if (sort_customer_name != null && !sort_customer_name.isEmpty()) {
			sortValue = a -> a.getCustomerName();
			asc = sort_customer_name.equals("asc");
		} else if (sort_division_name != null && !sort_division_name.isEmpty()) {
			sortValue = a -> a.getDivisionName();
			asc = sort_division_name.equals("asc");
		} else if (sort_sign_status != null && !sort_sign_status.isEmpty()) {
			sortValue = a -> a.getSignStatus();
			asc = sort_sign_status.equals("asc");
		} else if (sort_client_type_name_short != null && !sort_client_type_name_short.isEmpty()) {
			sortValue = a -> a.getClientTypeNameShort();
			asc = sort_client_type_name_short.equals("asc");
		} else if (sortPlanDate != null && !sortPlanDate.isEmpty()) {
			sortValue = a -> a.getPlanDate();
			asc = sortPlanDate.equals("asc");
		} else if (sortFileExists != null && !sortFileExists.isEmpty()) {
			sortValue = a -> a.getFileExists();
			asc = sortFileExists.equals("asc");
		} else if (sortPaid != null && !sortPaid.isEmpty()) {
			sortValue = a -> a.getPaid();
			asc = sortPaid.equals("asc");
		} else if (sortAccepted != null && !sortAccepted.isEmpty()) {
			sortValue = a -> a.getAccepted();
			asc = sortAccepted.equals("asc");
		} else if (!empty(sortJobs)) {
			sortValue = a -> a.getJoinJobNames();
			asc = sortJobs.equals("asc");
		} else {
			sortValue = a -> a.getDate();
			if (sort_date != null && !sort_date.isEmpty()) {
				asc = sort_date.equals("asc");
			} else {
				asc = false;
			}
		}
		return (a1, a2) -> {
			int i = nullFirstCompare(sortValue.apply(a1), sortValue.apply(a2), asc);
			return i == 0 ? a1.getId().compareTo(a2.getId()) : i;
		};
	}

	private CompletableFuture<Object> listContracts(AuthenticatedUser authUser, String periodFrom, String periodTo,
			String filter_number, String filter_division_name, String filter_customer_name, String filter_sign_status,
			String sort_date, String sort_number, String sort_customer_name, String sort_division_name,
			String sort_sign_status, String filter_client_type_name_short, String sort_client_type_name_short,
			String sortPlanDate, String filterFileExists, String sortFileExists, String filterPaid, String sortPaid,
			String filterAccepted, String sortAccepted, String filterJobs, String sortJobs,
			String processingStatusFilter, String filterPlanDateFrom, String filterPlanDateTo, String filterRequestId,
			String output, String filterCustomerId, String filterHasComments, String filterDivisionId,
			String filterWithoutPayment, String paymentFirstDateFrom, String paymentFirstDateTo, String start,
			String count, String filterAcceptAccountingPeriodFrom, String filterAcceptAccountingPeriodTo,
			String filterCreatorDivId) {
		String dateBefore = periodTo == null || periodTo.isEmpty() ? null
				: LocalDate.parse(periodTo).plusDays(1).toString();
		String paymentsFirstDateBefore = empty(paymentFirstDateTo) ? null
				: LocalDate.parse(paymentFirstDateTo).plusDays(1).toString();
		CompletableFuture<Set<String>> divsf = divisions.grantedDivs(authUser);
		CompletableFuture<Object> result = divsf.thenCompose(divs -> {
			return contracts.select().thenApply(stream -> {
				List<Contract> cs = stream.filter(contractUserFilter(divs))
						.filter(a -> periodFrom == null || a.getDate().compareTo(periodFrom) >= 0)
						.filter(a -> dateBefore == null || a.getDate().compareTo(dateBefore) < 0)
						.filter(a -> empty(filterPlanDateFrom)
								|| (!empty(a.getPlanDate()) && a.getPlanDate().compareTo(filterPlanDateFrom) >= 0))
						.filter(a -> empty(filterPlanDateTo)
								|| (!empty(a.getPlanDate()) && a.getPlanDate().compareTo(filterPlanDateTo) <= 0))
						.filter(likeFilter(filter_number, Contract::getNumber))
						.filter(likeFilter(filter_customer_name, Contract::getCustomerName))
						.filter(likeFilter(filterCustomerId, Contract::getCustomerId))
						.filter(likeFilter(filter_division_name, Contract::getDivisionName))
						.filter(likeFilter(filterDivisionId, Contract::getDivisionId))
						.filter(likeFilter(filter_sign_status, Contract::getSignStatus))
						.filter(likeFilter(filter_client_type_name_short, Contract::getClientTypeNameShort))
						.filter(likeFilter(filterFileExists, Contract::getFileExists))
						.filter(likeFilter(filterPaid, Contract::getPaid))
						.filter(likeFilter(filterAccepted, Contract::getAccepted))
						.filter(a -> empty(filterHasComments) || ("+".equals(filterHasComments) && a.getHasComments()))
						.filter(likeFilter(filterJobs, Contract::getJoinJobNames))
						.filter(likeFilter(filterRequestId, Contract::getRequestId))
						.filter(a -> empty(filterWithoutPayment)
								|| (filterWithoutPayment.equals("1") && a.getWithoutPayment())
								|| (filterWithoutPayment.equals("0") && !a.getWithoutPayment()))
						.filter(a -> between(a.getPaymentsFirstDate(), paymentFirstDateFrom, paymentsFirstDateBefore))
						.filter(a -> empty(filterAcceptAccountingPeriodFrom) || a.getAccountingPeriods()
								.filter(p -> p.compareTo(filterAcceptAccountingPeriodFrom) >= 0).count() > 0)
						.filter(a -> empty(filterAcceptAccountingPeriodTo) || a.getAccountingPeriods()
								.filter(p -> p.compareTo(filterAcceptAccountingPeriodTo) <= 0).count() > 0)
						.filter(a -> empty(filterCreatorDivId) || (a.getRequest() != null
								&& filterCreatorDivId.equals(a.getRequest().getCreatorDivisionId())))
						.collect(Collectors.toList());
				if (empty(filterRequestId)) {
					Stream<Contract> cst = cs.stream()
							.filter(a -> "Все".equals(processingStatusFilter)
									|| likeFilters(empty(processingStatusFilter) ? "В работе" : processingStatusFilter,
											a.getProcessingStatus()))
							.sorted(buildComparator(sort_date, sort_number, sort_customer_name, sort_division_name,
									sort_sign_status, sort_client_type_name_short, sortPlanDate, sortFileExists,
									sortPaid, sortAccepted, sortJobs));
					if (!empty(start)) {
						cst = cst.skip(Long.parseLong(start));
					}
					if (!empty(count)) {
						cst = cst.limit(Long.parseLong(count));
					}
					cs = cst.collect(Collectors.toList());
				}
				return cs;
			});
		});
		Objects.requireNonNull(result, "result is null");
		return result;
	}

	public CompletableFuture<Stream<ComboOption>> getCustomersCombo(WebixGridQuery q) {
		String clientTypeId = q.getFilter("client_type_id");
		String filterValue = q.getFilter("value");
		if (empty(clientTypeId)) {
			throw new BadRequest();
		}
		Function<AuthenticatedUser, CompletableFuture<Stream<ComboOption>>> function;
		switch (clientTypeId) {
		case Customers.CLIENT_TYPE_LEGAL:
			function = authUser -> getCustomersComboLegal(authUser, filterValue);
			break;
		case Customers.CLIENT_TYPE_REAL:
			function = authUser -> getCustomersComboReal(authUser, filterValue);
			break;
		default:
			logger.info("bad client_type_id:" + clientTypeId);
			throw new BadRequest();
		}
		return function.apply(q.getAuthUser());
	}

	private CompletableFuture<Stream<ComboOption>> getCustomersComboLegal(AuthenticatedUser user, String filterValue) {
		return customers
				.selectFiltered(
						user)
				.thenApply(stream -> stream
						.filter(customer -> filterValue == null
								|| customer.getName().toLowerCase().contains(filterValue.toLowerCase())
								|| customer.getTinFull().toLowerCase().contains(filterValue.toLowerCase()))
						.limit(10).map(customer -> new ComboOption(customer.getId(), customer.getFull_name())));
	}

	private CompletableFuture<Stream<ComboOption>> getCustomersComboReal(AuthenticatedUser authUser,
			String filterValue) {
		return customers.selectReal(authUser)
				.thenApply(s -> s
						.filter(customer -> filterValue == null
								|| notNull(customer.getName(), "").toLowerCase().contains(filterValue.toLowerCase())
								|| notNull(customer.getPhone(), "").toLowerCase().contains(filterValue.toLowerCase()))
						.map(rc -> new ComboOption(rc.getId(), rc.getName() + " " + rc.getPhone())).limit(10));
	}

	private CompletableFuture<List<RequestsPrice>> getRequestsPricesServices(WebixGridQuery query) {

		String divisionId = query.getFilter("divisionId");
		String objectTypeId = query.getFilter("objectTypeId");
		String objectParameterId = query.getFilter("objectParameterId");
		String clientTypeId = query.getFilter("clientTypeId");

		if (empty(divisionId) || empty(objectParameterId) || empty(objectParameterId) || empty(clientTypeId)) {
			throw new BadRequest();
		}

		return requestsInternal.getServices(divisionId, objectTypeId, objectParameterId, clientTypeId)
				.thenApply(list -> {
					// перепаковка, потому что пришедший RequestsPrice это строки прайса
					// и портить их не надо
					// с другой стороны clientTypeId был указан явно и голову морочить людям с
					// calcParamsR и calcParamsL тоже ни к чему
					List<RequestsPrice> clones = new ArrayList<>(list.size());
					for (RequestsPrice orig : list) {
						RequestsPrice clone = new RequestsPrice();
						clone.setId(orig.getId());
						if (clientTypeId.equals(Customers.CLIENT_TYPE_REAL)) {
							clone.setPrice(orig.getPrice());
							clone.setDurationWorkDays(orig.getDurationWorkDays());
							clone.setCalcTemplateId(orig.getCalcTemplateRealId());
							clone.setCalcParams(orig.getCalcParamsReal());
						} else if (clientTypeId.equals(Customers.CLIENT_TYPE_LEGAL)) {
							clone.setPrice(orig.getPriceLegal());
							clone.setDurationWorkDays(orig.getDurationWorkDaysLegal());
							clone.setCalcTemplateId(orig.getCalcTemplateLegalId());
							clone.setCalcParams(orig.getCalcParamsLegal());
						} else {
							throw new BadRequest();
						}
						clone.setServiceId(orig.getServiceId());
						clone.setService(orig.getService());
						clone.setObjectTypeId(orig.getObjectTypeId());
						clone.setObjectTypeName(orig.getObjectTypeName());
						clone.setObjectParameterId(orig.getObjectParameterId());
						clone.setObjectParameterValue(orig.getObjectParameterValue());
						clone.setSelected(orig.getSelected());
						clones.add(clone);
					}
					return clones;
				});
	}

	private CompletableFuture<File> postRequestsDownloadCalc(AuthenticatedUser authUser, CalculateRequest cr) {
		if (cr == null || empty(cr.getCalcTemplateId())) {
			throw new BadRequest();
		}
		if (empty(cr.getCalcParams())) {
			return files.get(cr.getCalcTemplateId());
		} else {
			return files.get(cr.getCalcTemplateId()).thenApply(f -> {
				try {
					Map<String, String> params = new HashMap<>();
					for (CalcParam p : cr.getCalcParams()) {
						params.put(p.getName(), p.getValue());
					}
					logger.info("calcParams: " + Tools.JSON.write(params));
					return new File(f.getFilename(), xlsReader.exporttoXLS(f.getBytes(), params), f.getFiletype());
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	private CompletableFuture<NextNumberResult> nextVatInvoiceNumber(AuthenticatedUser authUser, String divisionId,
			String date) {
		if (empty(divisionId) || empty(date)) {
			throw new BadRequest();
		}
		return divisions.grantedDivs(authUser).thenCompose(divs -> {
			if (divs.contains(divisionId)) {
				// счет-фактуры 5904/1805/000002
				return docs.nextNumber(authUser, divisionId, VAT_INVOICE_DOC_TYPE_ID, date)
						.thenApply(nextNumber -> new NextNumberResult(nextNumber));
			} else {
				throw new Forbidden();
			}
		});
	}

	private CompletableFuture<DocNumerator> getNumerator(AuthenticatedUser authUser, String divisionId,
			String docTypeId, String clientTypeId,String period) {
		if (authUser.isAdmin()) {
			if (empty(divisionId) | empty(docTypeId) || empty(period)) {
				throw new BadRequest();
			}
			return docs.getNumerator(authUser, divisionId, docTypeId, clientTypeId,period);
		} else {
			throw new Forbidden();
		}
	}

	private CompletableFuture<UpdateResult> setNumerator(AuthenticatedUser authUser,DocNumerator input){
    	if(authUser.isAdmin()) {
    		String divisionId=input.getDivisionId();
    		String docTypeId=input.getDocTypeId();
    		String period=input.getPeriod();
    		if(input==null||empty(divisionId)||empty(docTypeId)||empty(period)) {
    			throw new BadRequest();
    		}
    		return docs.getNumerator(authUser, divisionId, docTypeId, input.getClientTypeId(),period).thenCompose(storedNumerator->{
    			storedNumerator.setNextNumber(input.getNextNumber());
    			return docs.setNumerator(authUser, storedNumerator).thenApply(v->UpdateResult.SUCCESS_RESULT);
    		});
    	}else {
    		throw new Forbidden();
    	}
    }

	private CompletableFuture<List<ComboOption>> getSignatories(AuthenticatedUser authUser, String divisionId,
			String docTypeId) {
		if (empty(divisionId) || empty(docTypeId)) {
			throw new BadRequest();
		}
		// docs.getSignatories проверит доступы по подразделениям
		return docs.getSignatories(authUser, divisionId, docTypeId);
	}
}
