package ru.rosinv.server.file;

import java.util.concurrent.CompletableFuture;

import ru.rosinv.server.security.File;

public interface Files {
	public CompletableFuture<File> get(String id);
}
