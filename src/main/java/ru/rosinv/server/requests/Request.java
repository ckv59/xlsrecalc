package ru.rosinv.server.requests;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ru.rosinv.server.comments.CommentableEntity;
import ru.rosinv.server.customer.Customer;
import ru.rosinv.server.customer.RealCustomer;
import ru.rosinv.server.division.Division;
import ru.rosinv.server.docs.ClientType;
import ru.rosinv.server.requests.objecttype.RequestsObjectType;
import ru.rosinv.server.requests.price.RequestsPrice;

/**
 * Обращение
 */
public class Request extends CommentableEntity{

    /**
     * uuid
     */
    private String id;
    private Integer number;
    private String date;
    private String status;// статус удаленности active/removed
    private Division division;
    private ClientType clientType;
    private Customer customer;
    private RealCustomer realCustomer;// физическое лицо
    private String goal;// цель обращения
    private RequestsObjectType objectType;
    private String objectAddress;
    private List<RequestServiceEntry> services;
    private BigDecimal amount;
    private String objectParameterName;
    private String objectParameterId;
    private List<RequestsPrice> prices;
    /*
    статус по процессу,
    Временный - это в процессе переговоров, договор может быть заключен, а может быть и нет
    Договор - оформлен договор
    дальше статусы пока не явны
     */
    private String requestStatus;// статус по процессу, 
    private RequestData info;// данные обращения, адрес и файлы
    private String serviceNames;
    private String contractNumbers;
    private String userId;
    private String creator_div_id;
    
    public String getGoal() {
    	return goal;
    }
    
    public List<RequestServiceEntry> getServices(){
    	return services;
    }
    
    public String getId() {
        return id;
    }

    public Integer getNumber() {
        return number;
    }

    public String getDate() {
        if (date == null) {
            return "";
        } else if (date.length() > 10) {
            return date.substring(0, 10);
        }else{
            return date;
        }
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Division getDivision() {
        return division;
    }

    public String getDivisionName() {
        return division == null ? "" : division.getName();
    }

    public String getDivisionId() {
        return division == null ? "" : division.getId();
    }

    public void setDivision(Division division) {
        this.division = division;
    }

    public ClientType getClientType() {
        return clientType;
    }

    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setUserId(String id) { this.userId = id; }

    public String getCustomerId() {
        if (realCustomer != null) {
            return realCustomer.getId();
        } else if (customer != null) {
            return customer.getId();
        } else {
            return null;
        }
    }

    public RealCustomer getRealCustomer() {
        return realCustomer;
    }

    public void setRealCustomer(RealCustomer realCustomer) {
        this.realCustomer = realCustomer;
    }

    public String getCustomerName() {
        if (customer != null) {
            return customer.getName();
        } else if (realCustomer != null) {
            return realCustomer.getName() + realCustomer.getPhone();
        } else {
            return null;
        }
    }

    public RequestsObjectType getObjectType() {
        return objectType;
    }

    public String getObjectTypeId() {
        return objectType == null ? null : objectType.getId();
    }

    public String getObjectTypeName() {
        return objectType == null ? null : objectType.getName();
    }

    public void setObjectType(RequestsObjectType objectType) {
        this.objectType = objectType;
    }

    public String getObjectAddress() {
        return objectAddress != null ? objectAddress : (info == null ? "" : info.getAddress());
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Map<String, String> getFiles() {
        if (info == null) {
            return new HashMap<>();
        } else {
            return info.getFiles();
        }
    }

    public List<RequestsPrice> getPrices() {
        return prices;
    }

    public String getClientTypeId() {
        return clientType == null ? null : clientType.getId();
    }

    public String getClientTypeName() {
        return clientType == null ? null : clientType.getName();
    }

    public String getClientTypeShortName() {
        return clientType == null ? null : clientType.getShortName();
    }

    public String getServiceNames() {
        return this.serviceNames;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getAmountStr() {
        return amount == null ? "0.00" : amount.setScale(2, RoundingMode.HALF_UP).toPlainString();
    }

    public String getContractNumbers() {
        return contractNumbers;
    }

    public String getObjectParameterTypeName() {
        return objectType==null?null:objectType.getParameterName();
    }
    
    public String getObjectParameterValueName(){
        return objectParameterName;
    }
    
    public String getObjectParameterId() {
    	return objectParameterId;
    }

    public String getUserId() { return userId; }
    
    public String getCreatorDivisionId() {
    	return creator_div_id;
    }
    
    public void setCreatorDivisionId(String divisionId) {
    	creator_div_id=divisionId;
    }
}
