package ru.rosinv.server.requests;

import java.util.HashMap;
import java.util.Map;

public class RequestData {

    private String address;
    private Map<String, String> files; // docTypeId -> fileId

    public Map<String, String> getFiles() {
        if (files == null) {
            files = new HashMap<>();
        }
        return files;
    }

    public String getAddress() {
        return address;
    }
}
