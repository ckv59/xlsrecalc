package ru.rosinv.server.requests;

public class RequestFile {

    private String docTypeId;
    private String docTypeName;
    private String fileId;
    
    public String getDocTypeId() {
    	return docTypeId;
    }
    
    public String getDocTypeName() {
    	return docTypeName;
    }
    
    public String getFileId() {
    	return fileId;
    }
}
