package ru.rosinv.server.requests;

import java.math.BigDecimal;
import java.util.List;
import ru.rosinv.server.requests.doctype.RequestsDocType;

public class RequestServiceEntry {

    private String serviceId;
    private String serviceName;
    private BigDecimal price;
    private Integer durationWorkDays;
    private List<RequestsDocType> fullDocTypes;
    private List<RequestFile> files;

    public String getServiceId() {
        return serviceId;
    }
    
    public String getServiceName() {
    	return serviceName;
    }
    
    public BigDecimal getPrice() {
    	return price;
    }
    
    public Integer getDurationWorkDays() {
    	return durationWorkDays;
    }
    
    public List<RequestsDocType> getFullDocTypes(){
    	return fullDocTypes;
    }
    
    public List<RequestFile> getFiles(){
    	return files;
    }
}
