package ru.rosinv.server.requests;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import ru.rosinv.server.requests.price.RequestsPrice;
import ru.rosinv.server.security.AuthenticatedUser;

public interface Requests {

    public CompletableFuture<Stream<Request>> getAllActive();

    public CompletableFuture<Void> update(AuthenticatedUser authUser, Request request);
    
    public CompletableFuture<List<RequestsPrice>> getServices(String divisionId,String objectTypeId,String objectParameterId,String clientTypeId);
}
