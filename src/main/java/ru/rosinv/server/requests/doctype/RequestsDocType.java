package ru.rosinv.server.requests.doctype;

import static ru.rosinv.server.requests.doctype.RequestsDocTypeContext.SERVICE;
import ru.rosinv.server.tools.Tools;

public class RequestsDocType {

    private String id;
    private String name;
    private String status;
    private Long updated;
    private RequestsDocTypeContext context;

    public String getId() {
        return id;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public String getName() {
        return name;
    }

    public Long getUpdated() {
        return updated;
    }

    public String getStatus() {
        return status;
    }

    public RequestsDocTypeContext getContext() {
        return Tools.notNull(context, SERVICE);
    }

    public void setContext(RequestsDocTypeContext context) {
        this.context = context;
    }

    public void setName(String name) {
        this.name = name;
    }

}
