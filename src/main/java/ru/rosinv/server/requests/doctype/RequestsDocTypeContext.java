package ru.rosinv.server.requests.doctype;

import ru.rosinv.server.division.ComboOption;

public class RequestsDocTypeContext extends ComboOption {
    public static final RequestsDocTypeContext CUSTOMER_REAL = new RequestsDocTypeContext("571b8e7f-f750-4def-b2d9-dc0b6f908b97", "Заказчик ФЛ");
    public static final RequestsDocTypeContext CUSTOMER_LEGAL = new RequestsDocTypeContext("322e08b5-b986-42ef-bdfd-8fc4587a9cd8", "Заказчик ЮЛ");
    public static final RequestsDocTypeContext OBJECT = new RequestsDocTypeContext("1ee7e2b0-ee43-42c5-a915-8e80a3c34f29", "Объект");
    public static final RequestsDocTypeContext SERVICE = new RequestsDocTypeContext("a17adb75-3b8b-4425-af57-f725f7617fc7", "Услуга");

    public RequestsDocTypeContext(){
        // reflection json
    }
    public RequestsDocTypeContext(String id, String title) {
        super(id, title);
    }

}
