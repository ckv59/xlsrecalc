package ru.rosinv.server.requests.objecttype;

import java.util.List;

public class RequestsObjectType {

    private String id;
    private String name;
    private String status;
    private Long updated;
    private String parameterName;
    private List<RequestsObjectTypeParameter> parameterValues;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public String getParameterName() {
        return parameterName;
    }
    
    public List<RequestsObjectTypeParameter> getParameterValues(){
    	return parameterValues;
    }

}
