package ru.rosinv.server.requests.objecttype;

public class RequestsObjectTypeParameter {

    private String id;
    private String name;
    
    public String getId() {
    	return id;
    }
    
    public String getName() {
    	return name;
    }
}
