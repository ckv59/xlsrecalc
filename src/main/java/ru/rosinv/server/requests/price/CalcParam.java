package ru.rosinv.server.requests.price;

public class CalcParam {

    private String id;
    private String name;
    private String unit;
    private String value;
    private String min;
    private String max;

    public CalcParam() {

    }

    public CalcParam(String id, String name, String unit, String min, String max) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.min = min;
        this.max = max;
    }

    public CalcParam(String id, String name, String unit, String value) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.value = value;
    }

    public String getId() {
    	return id;
    }
    
    public String getUnit() {
    	return unit;
    }
    
    public String getMin() {
    	return min;
    }
    
    public String getMax() {
    	return max;
    }
    
    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
