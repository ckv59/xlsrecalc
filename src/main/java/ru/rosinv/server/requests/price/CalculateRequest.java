package ru.rosinv.server.requests.price;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CalculateRequest {

    private List<CalcParam> calcParams;
    private String calcTemplateId;

    public List<CalcParam> getCalcParams() {
        return calcParams;
    }

    public String getCalcTemplateId() {
        return calcTemplateId;
    }

    public void set(Map<String, String> params) {
        for (Map.Entry<String, String> e : params.entrySet()) {
            String name = e.getKey();
            String value = e.getValue();
            boolean found = false;
            for (CalcParam p : calcParams) {
                if (name.equals(p.getName())) {
                    p.setValue(value);
                    found = true;
                    break;
                }
            }
            if (!found) {
                calcParams.add(new CalcParam(UUID.randomUUID().toString(), name, "", value));
            }
        }
    }

}
