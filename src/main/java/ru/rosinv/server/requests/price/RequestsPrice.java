package ru.rosinv.server.requests.price;

import java.math.BigDecimal;
import java.util.List;
import ru.rosinv.server.contracts.jobtype.JobType;
import ru.rosinv.server.requests.service.RequestsService;

public class RequestsPrice {

	private String id;
	private BigDecimal price;// для физических лиц
	private BigDecimal priceL;// для юридических лиц
	private Integer durationWorkDays;// продолжительность выполнения работ в рабочих днях, физических лиц
	private Integer durationWorkDaysL;// продолжительность выполнения работ в рабочих днях, физических лиц
	private String serviceId;
	private String serviceName;
	private String serviceFullName;
	private String objectTypeId;
	private String objectTypeName;
	private String objectParameterId;
	private String objectParameterValue;
	private RequestsService service;
	private Integer selected; // для интерфейса, там с галочками надо показывать
	private String calcTemplateR;// образец сметы для ФЛ
	private String calcTemplateL;// образец сметы для ЮЛ
	private String calcTemplateId; // смета используемая в этом обращении
	// если параметров нет, то это просто образец, который надо прямо отдать
	private List<CalcParam> calcParamsR; // параметры сметы для ФЛ
	private List<CalcParam> calcParamsL; // параметры сметы для ЮЛ
	private List<CalcParam> calcParams; // сохраненные параметры сметы конкретного обращения

	public String getCalcTemplateId() {
		return calcTemplateId;
	}

	public List<CalcParam> getCalcParamsReal() {
		return calcParamsR;
	}

	public List<CalcParam> getCalcParamsLegal() {
		return calcParamsL;
	}

	public List<CalcParam> getCalcParams() {
		return calcParams;
	}

	public String getId() {
		return id;
	}

	public BigDecimal getPriceLegal() {
		return priceL;
	}

	public Integer getDurationWorkDaysLegal() {
		return durationWorkDaysL;
	}

	public String getServiceName() {
		return serviceName;
	}

	public String getServiceFullName() {
		return serviceFullName;
	}

	public String getObjectTypeName() {
		return objectTypeName;
	}

	public String getObjectParameterValue() {
		return objectParameterValue;
	}

	public Integer getSelected() {
		return selected;
	}

	public String getCalcTemplateRealId() {
		return calcTemplateR;
	}

	public String getCalcTemplateLegalId() {
		return calcTemplateL;
	}

	public String getObjectTypeId() {
		return objectTypeId;
	}

	public String getObjectParameterId() {
		return objectParameterId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setService(RequestsService service) {
		this.service = service;
		this.serviceName = service.getName();
		this.serviceFullName = service.getFullName();
	}

	public RequestsService getService() {
		return service;
	}

	public JobType getContractsJobType() {
		return service == null ? null : service.getContractsJobType();
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getDurationWorkDays() {
		return durationWorkDays;
	}

	public void setDurationWorkDays(Integer durationWorkDays) {
		this.durationWorkDays = durationWorkDays;
	}

	public String getClientTypeId() {
		return service == null ? null : service.getClientTypeId();
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public void setObjectTypeId(String objectTypeId) {
		this.objectTypeId = objectTypeId;
	}

	public void setObjectTypeName(String objectTypeName) {
		this.objectTypeName = objectTypeName;
	}

	public void setObjectParameterId(String objectParameterId) {
		this.objectParameterId = objectParameterId;
	}

	public void setObjectParameterValue(String objectParameterValue) {
		this.objectParameterValue = objectParameterValue;
	}

	public void setSelected(Integer selected) {
		this.selected = selected;
	}

	public void setCalcTemplateId(String calcTemplateId) {
		this.calcTemplateId = calcTemplateId;
	}

	public void setCalcParams(List<CalcParam> calcParams) {
		this.calcParams = calcParams;
	}
}
