package ru.rosinv.server.requests.service;

import ru.rosinv.server.requests.doctype.RequestsDocType;

public class RequestServiceDoc extends RequestsDocType {

    private Integer checked;
    private Integer required;
    
    public Integer getChecked() {
    	return checked;
    }
    
    public Integer getRequired() {
    	return required;
    }
}
