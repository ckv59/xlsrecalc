package ru.rosinv.server.requests.service;

import java.util.ArrayList;
import java.util.List;

/**
 * Подробности услуги для конкретных видов объектов
 */
public class RequestServiceObjectTypeInfo {

    private String id;
    private List<RequestServiceDoc> requiredDocs;
    private List<RequestServiceDoc> resultDocs;

    public String getId() {
        return id;
    }

    public List<RequestServiceDoc> getResultDocs() {
        if(resultDocs==null){
            resultDocs=new ArrayList<>();
        }
        return resultDocs;
    }

    public List<RequestServiceDoc> getRequiredDocs() {
        if (requiredDocs == null) {
            requiredDocs = new ArrayList<>();
        }
        return requiredDocs;
    }
}
