package ru.rosinv.server.requests.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import java.util.stream.Stream;
import ru.rosinv.server.contracts.jobtype.JobType;
import ru.rosinv.server.docs.ClientType;
import static ru.rosinv.server.tools.Tools.empty;

/**
 * Услуга, котируемая по прайсу *
 */
public class RequestsService {

    private String id;
    private String name;
    private String status;
    private Long updated;
    /**
     * виды объектов, к которым применима услуга, с данными по документам, какие
     * есть
     */
    private List<RequestServiceObjectTypeInfo> types;
    /**
     * идентификатор вида работа из договоров
     */
    private JobType contractsJobType;
    private ClientType clientType;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public JobType getContractsJobType() {
        return contractsJobType;
    }

    public String getContractsJobTypeId() {
        return contractsJobType == null ? null : contractsJobType.getId();
    }

    public void setContractsJobType(JobType contractsJobType) {
        this.contractsJobType = contractsJobType;
    }

    public Set<String> getObjectTypes() {
        return getTypes().stream().map(RequestServiceObjectTypeInfo::getId).collect(toSet());
    }

    public List<RequestServiceObjectTypeInfo> getTypes() {
        if (types == null) {
            types = new ArrayList<>();
        }
        return types;
    }

    private Stream<RequestServiceObjectTypeInfo> getObjectTypeInfo(String objectTypeId) {
        return getTypes().stream().filter(t -> Objects.equals(t.getId(), objectTypeId));
    }

    public List<String> getResultDocNames(String objectTypeId) {
        return getObjectTypeInfo(objectTypeId)
                .flatMap(oti -> oti.getResultDocs().stream())
                .map(RequestServiceDoc::getName)
                .collect(toList());
    }

    public List<String> getRequiredDocsNames(String objectTypeId) {
        return getObjectTypeInfo(objectTypeId)
                .flatMap(oti -> oti.getRequiredDocs().stream())
                .map(RequestServiceDoc::getName)
                .collect(toList());
    }

    public String getClientTypeId() {
        return clientType == null ? null : clientType.getId();
    }

    public String getClientTypeName() {
        return clientType == null ? null : clientType.getName();
    }
    
    public String getClientTypeShortName() {
        return clientType == null ? null : clientType.getShortName();
    }
    
    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }
    
    public String getFullName(){
        return name+(clientType==null||empty(clientType.getShortName())?"":" "+clientType.getShortName());
    }
}
