package ru.rosinv.server.security;

import java.util.Map;
import ru.rosinv.server.tools.HttpServerFunction;

public interface ApiComponent {
    public void start();
    public void stop();
    public Map<String, HttpServerFunction<? extends Object>> getRoutes();
}
