package ru.rosinv.server.security;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import static ru.rosinv.server.security.Roles.BOSS_ROLE_ID;
import static ru.rosinv.server.security.Roles.DEPUTY_BOSS_ROLE_ID;
import static ru.rosinv.server.security.Roles.OPERATOR_ROLE_ID;

public class AuthenticatedUser {

    public static final AuthenticatedUser SYSTEM = new AuthenticatedUser("258be2eb-5a22-43ce-9750-08d6cd6898d7", "system", new String[0], 0, null, null);
    public static final AuthenticatedUser REPLICATOR = new AuthenticatedUser("040fa5b3-7139-4072-be61-60eb797a8696", "replicator", new String[0], 0, null, null);
    private static final String EXTERNAL_TRUSTED_ID = "20f04e88-82a5-47be-8420-b2c82b61d8b1";
    private final String userId;
    private final String login;
    private final List<String> roleIds;
    private final Long expire;
    private final String divisionId;
    private final String info;

    public AuthenticatedUser(String userId, String login, String[] roleIds, long expire, String divisionId, String info) {
        this.userId = userId;
        this.roleIds = Arrays.asList(roleIds);
        this.expire = expire;
        this.divisionId = divisionId;
        this.info = info;
        this.login = login;
    }

    public String getUserId() {
        return userId;
    }

    public UUID getUuid() {
        return UUID.fromString(userId);
    }

    public Long expired() {
        return expire;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public String getInfo() {
        return info;
    }

    public boolean hasRole(String role_id) {
        return roleIds.contains(role_id);
    }

    public boolean hasAnyRole(String... roles) {
        for (String roleId : roles) {
            if (hasRole(roleId)) {
                return true;
            }
        }
        return false;
    }

    public boolean isAdmin() {
        return hasRole(Roles.ADMIN_ROLE_ID);
    }

    public boolean isTestAdmin() {
        return hasRole(Roles.TEST_ADMIN_ROLE_ID);
    }

    public boolean isEconomist() {
        return hasRole(Roles.ECONOMIST_ROLE_ID);
    }

    public boolean isAccountant() {
        return hasRole(Roles.ACCOUNTANT_ROLE_ID);
    }

    public boolean isCurator() {
        return hasRole(Roles.CURATOR_ROLE_ID);
    }

    public boolean isOperatorOrDeputyBoss() {
        return hasAnyRole(OPERATOR_ROLE_ID, DEPUTY_BOSS_ROLE_ID, BOSS_ROLE_ID);
    }

    public boolean isDeputyBoss() {
        return hasAnyRole(DEPUTY_BOSS_ROLE_ID, BOSS_ROLE_ID);
    }

    public boolean isBoss() {
        return hasRole(Roles.BOSS_ROLE_ID);
    }

    public String getLogin() {
        return login;
    }

    public boolean isReplicator() {
        return this == AuthenticatedUser.REPLICATOR;
    }

    public boolean isExternalTrustedSystem() {
        return Objects.equals(userId, EXTERNAL_TRUSTED_ID);
    }
}
