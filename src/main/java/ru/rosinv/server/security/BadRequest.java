package ru.rosinv.server.security;

public class BadRequest extends RuntimeException implements HttpError {

	private static final long serialVersionUID = -7226972334583346522L;

	@Override
	public int getStatusCode() {
		return 400;
	}

}
