package ru.rosinv.server.security;

public class File {

    private String filename;
    private byte[] filebytes;
    private String filetype;

    public File() {

    }

    public File(String filename, byte[] bytes, String filetype) {
        this.filename = filename;
        this.filebytes = bytes;
        this.filetype = filetype;
    }

    public String getFilename() {
        return filename;
    }

    public byte[] getBytes() {
        return filebytes;
    }

    public String getFiletype() {
        return filetype;
    }
}
