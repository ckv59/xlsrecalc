package ru.rosinv.server.security;

public class Forbidden extends RuntimeException implements HttpError{

	private static final long serialVersionUID = 5378784322097072572L;

	@Override
	public int getStatusCode() {
		return 403;
	}
    
}
