package ru.rosinv.server.security;

public interface HttpError {
	public int getStatusCode();
}
