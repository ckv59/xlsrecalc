package ru.rosinv.server.security;

import java.util.ArrayList;
import java.util.List;
import ru.rosinv.server.division.ComboOption;

public class Roles {

    /**
     * Администратор
     */
    public final static String ADMIN_ROLE_ID = "08c5792a-dfb7-490d-b477-aafff7fd7ce8";
    // Для разработки, в интерфейсе как админ, а к данным доступ ограничен.
    public final static String TEST_ADMIN_ROLE_ID = "fce92c58-bb43-4f15-bea8-f4793c3ded2a";
    /**
     * Оператор
     */
    public final static String OPERATOR_ROLE_ID = "1d02360d-52d1-4f02-bad9-f4f22260f6a9";
    /**
     * Руководитель
     */
    public final static String BOSS_ROLE_ID = "01ae8aa1-9835-4b8a-8c38-ef85e46aa5b4";
    /**
     * Заместитель руководителя
     */
    public final static String DEPUTY_BOSS_ROLE_ID = "4a9eed13-87a4-434c-8aa0-f00b90c18431";
    /**
     * Куратор
     */
    public final static String CURATOR_ROLE_ID = "3363c150-9d1c-4e22-bda1-741439984b65";
    /**
     * Бухгалтер
     */
    public final static String ACCOUNTANT_ROLE_ID = "062a2cd3-ee77-4ad5-8696-0e2e3b6835ad";
    /**
     * Экономист, планово-экономический отдел
     */
    public final static String ECONOMIST_ROLE_ID = "b42ff28c-6765-4396-bb9c-9c70eaf5ed5e";
    /**
     * Консультант
     */
    public final static String CONSULTANT_ROLE_ID = "e293f962-3101-476a-9f5a-73ecc0a5c243";
    private final List<ComboOption> comboList = new ArrayList<>();

    public Roles() {
        comboList.add(new ComboOption(ADMIN_ROLE_ID, "Администратор"));
        comboList.add(new ComboOption(BOSS_ROLE_ID, "Начальник подразделения"));
        comboList.add(new ComboOption(DEPUTY_BOSS_ROLE_ID, "Заместитель начальника подразделения"));
        comboList.add(new ComboOption(OPERATOR_ROLE_ID, "Оператор"));
        comboList.add(new ComboOption(CURATOR_ROLE_ID, "Куратор"));
        comboList.add(new ComboOption(ACCOUNTANT_ROLE_ID, "Бухгалтерия"));
        comboList.add(new ComboOption(ECONOMIST_ROLE_ID, "Экономист"));
        comboList.add(new ComboOption(CONSULTANT_ROLE_ID, "Консультант"));
    }

    public List<ComboOption> forCombo(AuthenticatedUser authUser) {
        if (authUser.hasRole(ADMIN_ROLE_ID)) {
            return comboList;
        } else {
            throw new Forbidden();
        }
    }
}
