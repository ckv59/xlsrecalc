package ru.rosinv.server.tools;

import ru.rosinv.server.security.AuthenticatedUser;

public class AuthInput {

    private AuthenticatedUser authUser;

    public AuthenticatedUser getAuthUser() {
        return authUser;
    }

    public void setAuthUser(AuthenticatedUser authUser) {
        this.authUser = authUser;
    }
    
}
