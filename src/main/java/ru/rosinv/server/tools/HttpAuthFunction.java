package ru.rosinv.server.tools;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpAuthFunction<I extends AuthInput> extends HttpServerFunction<I> {

    private static final Logger logger = LogManager.getLogger();

    public HttpAuthFunction(Class<I> cls, Function<I, Object> function) {
        super(cls, cs -> {
            try {
                I input = cs.getInput() == null ? cls.newInstance() : cs.getInput();
                input.setAuthUser(cs.getAuthUser());
                Map<String, String> params = cs.getParameters();
                if (params != null) {
                    params.forEach((k, v) -> {
                        try {
                            int i = k.indexOf("[");
                            if (i > 0) {
                                String mapname = k.substring(0, i);
                                String keyname = k.substring(i + 1, k.indexOf("]"));
                                setMapValue(cls, input, mapname, keyname, v);
                            } else {
                                Field f = findField(cls, k);
                                f.setAccessible(true);
                                f.set(input, v);
                            }
                        } catch (Exception e) {
                            logger.info(e.getClass().getSimpleName() + " " + cls.getSimpleName() + "." + k);
                        }
                    });
                }
                return function.apply(input);
            } catch (ReflectiveOperationException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private static Field findField(Class<? extends Object> cls, String name) throws NoSuchFieldException {
        for (Field f : cls.getDeclaredFields()) {
            if (f.getName().equals(name)) {
                return f;
            }
        }
        Class<? extends Object> superclass = cls.getSuperclass();
        if (superclass == null) {
            throw new NoSuchFieldException(cls.getName() + "." + name);
        } else {
            return findField(superclass, name);
        }
    }

    private static void setMapValue(Class<? extends Object> cls, Object input, String mapName, String key, String value) throws Exception {
        Field f = findField(cls,mapName);
        f.setAccessible(true);
        @SuppressWarnings("unchecked")
		Map<String, String> map = (Map<String, String>) f.get(input);
        if (map == null) {
            map = new HashMap<>();
            f.set(input, map);
        }
        map.put(key, value);
    }
}
