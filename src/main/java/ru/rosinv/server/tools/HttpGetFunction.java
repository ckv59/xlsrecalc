package ru.rosinv.server.tools;

import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import ru.rosinv.server.security.AuthenticatedUser;

public class HttpGetFunction extends HttpServerFunction<Void> {

    public HttpGetFunction(Function<AuthenticatedUser, ? extends Object> function) {
        super(null, cs -> function.apply(cs.getAuthUser()));
    }

    public HttpGetFunction(BiFunction<AuthenticatedUser, Map<String, String>, ? extends Object> function) {
        super(null, cs -> function.apply(cs.getAuthUser(), cs.getParameters()));
    }
}
