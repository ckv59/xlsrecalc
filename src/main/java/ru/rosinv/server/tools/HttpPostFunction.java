package ru.rosinv.server.tools;

import java.util.Map;
import java.util.function.BiFunction;
import ru.rosinv.server.security.AuthenticatedUser;
import ru.rosinv.server.tools.TriFunction;

public class HttpPostFunction<I> extends HttpServerFunction<I> {

    public HttpPostFunction(Class<I> inputClass, BiFunction<AuthenticatedUser, I, Object> function) {
        super(inputClass, cs -> function.apply(cs.getAuthUser(), cs.getInput()));
    }

    public HttpPostFunction(Class<I> inputClass, TriFunction<AuthenticatedUser, I, Map<String, String>, Object> function) {
        super(inputClass, cs -> function.apply(cs.getAuthUser(), cs.getInput(), cs.getParameters()));
    }
}
