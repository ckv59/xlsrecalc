package ru.rosinv.server.tools;

import java.util.Collection;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.Part;

public interface HttpRequest {

    public String print(String s);

    public String sendStatusCode(int statusCode);

    public String getBody();

    public String sendJson(String s);

    public void sendTextPlain(String s);

    public String getParameter(String name);

    public Map<String, String> getParameters();

    public String getRelativePath();

    public String getMethod();

    public Cookie getCookie(String name);

    public void setCookie(Cookie cookie);

    public Collection<Part> getParts();

    public String getContentType();

    public String sendFile(String filename, String contentType, byte[] bytes);

    public String sendContent(String contentType, byte[] bytes);

    public String getHeader(String headerName);
}
