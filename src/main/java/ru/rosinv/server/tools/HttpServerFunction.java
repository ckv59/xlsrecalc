package ru.rosinv.server.tools;

import ru.rosinv.server.security.Forbidden;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import static javax.servlet.http.HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE;
import javax.servlet.http.Part;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.rosinv.server.security.AuthenticatedUser;
import ru.rosinv.server.security.File;

public abstract class HttpServerFunction<I> {

    private static final Logger logger = LogManager.getLogger();

    protected class CallStructure {

        private final AuthenticatedUser authUser;
        private final I input;
        private final Map<String,String> parameters;

        private CallStructure(AuthenticatedUser authUser, I input, Map<String,String> parameters) {
            this.authUser = authUser;
            this.input = input;
            this.parameters = parameters;
        }

        protected AuthenticatedUser getAuthUser() {
            return authUser;
        }

        protected I getInput() {
            return input;
        }

        public Map<String,String> getParameters() {
            return parameters;
        }

    }
    private final Class<I> inputClass;
    private final Function<CallStructure, Object> function;
    private final Json json = new Json();

    public HttpServerFunction(Class<I> inputClass, Function<CallStructure, Object> function) {
        this.inputClass = inputClass;
        this.function = function;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public CompletableFuture<String> call(AuthenticatedUser authUser, HttpRequest request) {
        try {
            String method = request.getMethod();
            I input = null;
            if (method.equals("GET")) {
            } else if (method.equals("POST")) {
                String contentType = request.getContentType();
                if (contentType == null) {
                    logger.error("null content type");
                    return CompletableFuture.completedFuture(request.sendStatusCode(SC_UNSUPPORTED_MEDIA_TYPE));
                } else if (contentType.startsWith("application/json")) {
                    input = json.read(request.getBody(), inputClass);
                } else if (contentType.startsWith("multipart/form-data")) {
                    input = inputClass.newInstance();
                    for (Part part : request.getParts()) {
                        String name = part.getName();
                        byte[] bytes = new byte[Math.toIntExact(part.getSize())];
                        try (InputStream is = part.getInputStream()) {
                            is.read(bytes);
                        }
                        if (inputClass.equals(File.class)) {
                            input = (I) new File(part.getSubmittedFileName(), bytes, part.getContentType());
                        } else {
                            Field field = inputClass.getDeclaredField(name);
                            field.setAccessible(true);
                            Class<?> fieldType = field.getType();
                            if (fieldType.equals(String.class)) {
                                field.set(input, new String(bytes, StandardCharsets.UTF_8));
                            } else if (fieldType.equals(File.class)) {
                                field.set(input, new File(part.getSubmittedFileName(), bytes, part.getContentType()));
                            } else {
                                throw new Forbidden();
                            }
                        }
                    }
                } else {
                    logger.error("unsupported content type:'" + contentType + "'");
                    return CompletableFuture.completedFuture(request.sendStatusCode(SC_UNSUPPORTED_MEDIA_TYPE));
                }
            } else {
                throw new Forbidden();
            }
            Object result = function.apply(new CallStructure(authUser, input, request.getParameters()));
            CompletableFuture<Object> future;
            if (result instanceof CompletableFuture) {
            	future = (CompletableFuture<Object>) result;
            } else {
                future = CompletableFuture.completedFuture(result);
            }
            return future.thenApply(o -> {
                if (o instanceof String) {
                    return request.sendJson((String) o);
                } else if (o instanceof File) {
                    File f = (File) o;
                    return request.sendFile(f.getFilename(), f.getFiletype(), f.getBytes());
                } else if (o instanceof Stream) {
                    return request.sendJson(json.write(((Stream) o).collect(toList())));
                } else {
                    return request.sendJson(json.write(o));
                }
            });
        } catch (Exception e) {
            CompletableFuture<String> result = new CompletableFuture<String>();
            result.completeExceptionally(e);
            return result;
        }
    }
}
