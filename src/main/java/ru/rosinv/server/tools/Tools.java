package ru.rosinv.server.tools;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import ru.rosinv.server.division.ComboOption;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Tools {

    public static final BigDecimal HUNDRED = new BigDecimal(100);
    public static final BigDecimal VAT_PERCENT=new BigDecimal(18);
    public static final BigDecimal WVAT = HUNDRED.divide(new BigDecimal(118), 8, RoundingMode.HALF_UP); // коээфициент для вычисления без ндс из суммы с ндс
    public static final Json JSON = new Json();

    public static <T> Predicate<T> likeFilter(String filterValue, Function<T, String> getter) {
        return obj -> {
            if (filterValue == null || filterValue.isEmpty()) {
                return true;
            } else {
                Object dataValue = getter.apply(obj);
                if (dataValue == null) {
                    return false;
                } else {
                    return dataValue.toString().toLowerCase().contains(filterValue.trim().toLowerCase());
                }
            }
        };
    }

    public static <T> Predicate<T> equalOrEmpty(String filterValue, Function<T, String> getter) {
        return obj -> {
            if (filterValue == null || filterValue.isEmpty()) {
                return true;
            } else {
                String dataValue = getter.apply(obj);
                return dataValue == null || dataValue.isEmpty() || dataValue.equals(filterValue);
            }
        };
    }

    public static <T,T2 extends Comparable<T2>> Comparator<T> compare(Function<T, T2> getter, String asc) {
        return (o1, o2) -> {
            if (asc == null || asc.isEmpty()) {
                return 0;// а не надо сравнивать вообще
            }
            T2 v1 = getter.apply(o1);
            T2 v2 = getter.apply(o2);
            if (v1 == null) {
                return -1;
            } else if (v2 == null) {
                return 1;
            } else {
                int k = Objects.equals(asc, "asc") ? 1 : -1;
                return k * v1.compareTo(v2);
            }
        };
    }

    @SafeVarargs
	public static <T> Comparator<T> join(Comparator<T>... comparators) {
        return (o1, o2) -> {
            for (Comparator<T> c : comparators) {
                int i = c.compare(o1, o2);
                if (i != 0) {
                    return i;
                }
            }
            return 0;
        };
    }

    public static <T> List<ComboOption> toComboList(Stream<T> input, String filterValue, Function<T, String> idGetter, Function<T, String> valueGetter) {
        return input
                .filter(likeFilter(filterValue, valueGetter))
                .map(t -> new ComboOption(idGetter.apply(t), valueGetter.apply(t).toString()))
                .sorted(Comparator.comparing(ComboOption::getValue))
                .collect(Collectors.toList());
    }

    public static String isonow() {
        return ZonedDateTime.now().toString().substring(0, 10);
    }

    public static String isoMonthNow() {
        return ZonedDateTime.now().toString().substring(0, 7);
    }

    public static <T> T notNull(T t, T nullDefault) {
        return t == null ? nullDefault : t;
    }
    
    public static <T> Set<T> notNull(Set<T> set){
    	return notNull(set,new HashSet<>());
    }
    
    public static <T1,T2> Map<T1,T2> notNull(Map<T1,T2> map) {
        return notNull(map,new HashMap<>());
    }
    
    public static <T> List<T> notNull(List<T> list){
    	return list==null?new ArrayList<>():list;
    }

    public static String notEmpty(String t, String nullDefault) {
        return empty(t) ? nullDefault : t;
    }

    public static boolean empty(String s) {
        return s == null || s.isEmpty() || s.equals("null");
    }

    public static boolean empty(List<? extends Object> l) {
        return l == null || l.isEmpty();
    }

    private static final DateTimeFormatter rusformat = DateTimeFormatter.ofPattern("dd.MM.YYYY");

    public static String rusDateFormat(String isodate) {
        if (isodate == null) {
            return null;
        }
        if (isodate.isEmpty()) {
            return "";
        }
        return LocalDate.parse(isodate.substring(0, 10)).format(rusformat);
    }

    public static boolean equal(Object o1, Object o2) {
        return Objects.equals(o1, o2);
    }

    private static final Map<String, String> rusmonth = new HashMap<>();

    static {
        rusmonth.put("01", "январь");
        rusmonth.put("02", "февраль");
        rusmonth.put("03", "март");
        rusmonth.put("04", "апрель");
        rusmonth.put("05", "май");
        rusmonth.put("06", "июнь");
        rusmonth.put("07", "июль");
        rusmonth.put("08", "август");
        rusmonth.put("09", "сентябрь");
        rusmonth.put("10", "октябрь");
        rusmonth.put("11", "ноябрь");
        rusmonth.put("12", "декабрь");
    }

    public static final String monthRusWord(String isomonth) {
        return rusmonth.get(isomonth.substring(5, 7)) + " " + isomonth.substring(0, 4);
    }

    public static BigDecimal amountWithoutVAT(BigDecimal amount) {
        return amount.multiply(HUNDRED).divide(HUNDRED.add(VAT_PERCENT), 2,RoundingMode.HALF_UP);
    }

    public static BigDecimal VATfromAmount(BigDecimal amount) {
        return amount.multiply(VAT_PERCENT).divide(HUNDRED.add(VAT_PERCENT), 2,RoundingMode.HALF_UP);
    }

	public static <T extends Comparable<T>> int nullFirstCompare(T o1, T o2, boolean asc) {
		// в любом направлении вперед null и пустую строку, так как по хорошему их не
		// должно быть в сортируемом поле
		if (o1 == null) {
			return o2 == null ? 0 : -1;
		} else if (o2 == null) {
			return 1;
		} else if ((o1 instanceof String) && ((String) o1).isEmpty()) {
			return o1.compareTo(o2) == 0 ? 0 : -1;
		} else if ((o2 instanceof String) && ((String) o2).isEmpty()) {
			return 1;
		}
		return o1.compareTo(o2) * (asc ? 1 : -1);
	}

}
