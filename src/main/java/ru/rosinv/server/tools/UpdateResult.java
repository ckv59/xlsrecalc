package ru.rosinv.server.tools;

public class UpdateResult {

    private  boolean success;

    public static final UpdateResult SUCCESS_RESULT = new UpdateResult(true, null);
    private String error_field;

    public UpdateResult(){
        // reflections json
    }
    public UpdateResult(boolean success, String error_field) {
        this.success = success;
        this.error_field = error_field;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getError() {
        return error_field;
    }

}
