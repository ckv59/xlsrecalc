package ru.rosinv.server.tools;

import java.util.HashMap;
import java.util.Map;

public class WebixGridQuery extends AuthInput {

	private Map<String, String> filter;
	private Map<String, String> sort;
	private String start;
	private String count;

	public String getFilter(String key) {
		if (filter == null) {
			filter = new HashMap<>();
		}
		return filter.get(key);
	}

	public String getSort(String key) {
		if (sort == null) {
			sort = new HashMap<>();
		}
		return sort.get(key);
	}

	public String getStart() {
		return start;
	}

	public String getCount() {
		return count;
	}
}
