package ru.rosinv.server.tools;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;

public class Xls {

    public static HSSFCellStyle bodyNumericStyle(HSSFWorkbook wb) {
        return bodyStyle(wb, HorizontalAlignment.RIGHT, (short) 4);
    }

    public static HSSFCellStyle bodyPercentStyle(HSSFWorkbook wb) {
        return bodyStyle(wb, HorizontalAlignment.RIGHT, (short) 10);
    }

    public static HSSFCellStyle bodyDateStyle(HSSFWorkbook wb) {
        return bodyDateStyle(wb, HorizontalAlignment.LEFT);
    }

    public static HSSFCellStyle bodyDateStyle(HSSFWorkbook wb, HorizontalAlignment align) {
        HSSFFont h0f = wb.createFont();
        h0f.setBold(false);
        h0f.setFontName("Times New Roman");
        h0f.setFontHeightInPoints((short) 11);
        HSSFCellStyle h0s = wb.createCellStyle();
        h0s.setFont(h0f);
        h0s.setBorderBottom(BorderStyle.THIN);
        h0s.setBorderLeft(BorderStyle.THIN);
        h0s.setBorderRight(BorderStyle.THIN);
        h0s.setBorderTop(BorderStyle.THIN);
        h0s.setVerticalAlignment(VerticalAlignment.CENTER);
        h0s.setAlignment(align);
        h0s.setWrapText(true);
        h0s.setDataFormat((short) 14);
        return h0s;
    }

    public static HSSFCell createCell(HSSFSheet sheet, int row, int col, Object value, HSSFCellStyle style, int mergeRows, int mergeCols) {
        HSSFRow hrow = sheet.getRow(row);
        if (hrow == null) {
            hrow = sheet.createRow(row);
        }
        HSSFCell cell = hrow.createCell(col);
        if (value instanceof BigDecimal) {
            cell.setCellValue(((BigDecimal) value).doubleValue());
        } else if (value instanceof LocalDate) {
            LocalDate ld = (LocalDate) value;
            LocalDate ld0 = LocalDate.parse("1899-12-30");
            cell.setCellValue(ChronoUnit.DAYS.between(ld0, ld));
        } else if (value != null) {
            cell.setCellValue(value.toString());
        }
        cell.setCellStyle(style);
        if (mergeRows > 1 || mergeCols > 1) {
            sheet.addMergedRegion(new CellRangeAddress(row, row + mergeRows - 1, col, col + mergeCols - 1));
        }
        return cell;
    }

    public static HSSFCell createCell(HSSFSheet sheet, int row, int col, Object value, HSSFCellStyle style) {
        return createCell(sheet, row, col, value, style, 1, 1);
    }

    public static HSSFCellStyle headerStyle0(HSSFWorkbook wb, int fontHeight) {
        HSSFFont h0f = wb.createFont();
        h0f.setBold(true);
        h0f.setFontName("Times New Roman");
        h0f.setFontHeightInPoints((short) fontHeight);
        HSSFCellStyle h0s = wb.createCellStyle();
        h0s.setFont(h0f);
        h0s.setVerticalAlignment(VerticalAlignment.CENTER);
        h0s.setAlignment(HorizontalAlignment.CENTER);
        return h0s;
    }

    public static HSSFCellStyle headerStyle0(HSSFWorkbook wb) {
        return headerStyle0(wb, 12);
    }

    public static HSSFCellStyle headerStyle2(HSSFWorkbook wb) {
        return headerStyle2(wb, HorizontalAlignment.CENTER, BorderStyle.THIN);
    }

    public static HSSFCellStyle headerStyle2(HSSFWorkbook wb, HorizontalAlignment align, BorderStyle border) {
        return headerStyle2(wb, align, border, 11);
    }

    public static HSSFCellStyle headerStyle2(HSSFWorkbook wb, HorizontalAlignment align, BorderStyle border, int fontHeight) {
        return headerStyle2(wb, align, border, fontHeight, true);
    }

    public static HSSFCellStyle headerStyle2(HSSFWorkbook wb, HorizontalAlignment align, BorderStyle border, int fontHeight,boolean wrapText) {
        HSSFFont h0f = wb.createFont();
        h0f.setBold(true);
        h0f.setFontName("Times New Roman");
        h0f.setFontHeightInPoints((short) fontHeight);
        HSSFCellStyle h0s = wb.createCellStyle();
        h0s.setFont(h0f);
        h0s.setBorderBottom(border);
        h0s.setBorderLeft(border);
        h0s.setBorderRight(border);
        h0s.setBorderTop(border);
        h0s.setVerticalAlignment(VerticalAlignment.CENTER);
        h0s.setAlignment(align);
        h0s.setWrapText(wrapText);
        return h0s;
    }
    
    public static HSSFCellStyle bodyStyle(HSSFWorkbook wb) {
        return bodyStyle(wb, HorizontalAlignment.LEFT);
    }

    public static HSSFCellStyle bodyStyle(HSSFWorkbook wb, HorizontalAlignment align) {
        return bodyStyle(wb, align, (short) 0);
    }

    public static HSSFCellStyle bodyStyle(HSSFWorkbook wb, HorizontalAlignment align, short dataFormat) {
        HSSFFont h0f = wb.createFont();
        h0f.setBold(false);
        h0f.setFontName("Times New Roman");
        h0f.setFontHeightInPoints((short) 11);
        HSSFCellStyle h0s = wb.createCellStyle();
        h0s.setFont(h0f);
        h0s.setBorderBottom(BorderStyle.THIN);
        h0s.setBorderLeft(BorderStyle.THIN);
        h0s.setBorderRight(BorderStyle.THIN);
        h0s.setBorderTop(BorderStyle.THIN);
        h0s.setVerticalAlignment(VerticalAlignment.CENTER);
        h0s.setAlignment(align);
        h0s.setWrapText(true);
        h0s.setDataFormat(dataFormat);
        return h0s;
    }

    public static HSSFCellStyle textStyle(HSSFWorkbook wb, HorizontalAlignment align, int fontHeight) {
        return textStyle(wb, align, fontHeight, true);
    }

    public static HSSFCellStyle textStyle(HSSFWorkbook wb, HorizontalAlignment align, int fontHeight, boolean wrapText) {
        HSSFFont h0f = wb.createFont();
        h0f.setBold(false);
        h0f.setFontName("Times New Roman");
        h0f.setFontHeightInPoints((short) fontHeight);
        HSSFCellStyle h0s = wb.createCellStyle();
        h0s.setFont(h0f);
        h0s.setBorderBottom(BorderStyle.NONE);
        h0s.setBorderLeft(BorderStyle.NONE);
        h0s.setBorderRight(BorderStyle.NONE);
        h0s.setBorderTop(BorderStyle.NONE);
        h0s.setVerticalAlignment(VerticalAlignment.CENTER);
        h0s.setAlignment(align);
        h0s.setWrapText(wrapText);
        //h0s.setDataFormat(dataFormat);
        return h0s;
    }
}
