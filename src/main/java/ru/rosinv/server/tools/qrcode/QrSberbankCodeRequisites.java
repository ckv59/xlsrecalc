package ru.rosinv.server.tools.qrcode;

public class QrSberbankCodeRequisites {
    // public String CodeVersion;
    // public int CodePage;
    public String Name;
    public String PersonalAcc;
    public String BankName;
    public String BIC;
    public String CorrespAcc;
    public String PayeeINN;
    public String LastName;
    public String FirstName;
    public String MiddleName;
    public String PayerAddress;
    public String Purpose;
    public String Sum;
    public String paymPeriod;
    // public String TechCode;

    public String getText() {
       return String.format("ST00012|Name=%s|PersonalAcc=%s|BankName=%s|BIC=%s|CorrespAcc=%s|PayeeINN=%s|" +
                       "LastName=%s|FirstName=%s|MiddleName=%s|PayerAddress=%s|Purpose=%s|Sum=%s|paymPeriod=%s",
               Name, PersonalAcc, BankName, BIC, CorrespAcc, PayeeINN,
               LastName, FirstName, MiddleName, PayerAddress, Purpose, Sum, paymPeriod);
    }
}
