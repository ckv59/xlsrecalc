package ru.rosinv.server.tools.qrcode;

public class QrSpecialCodeValues {
    public String ContractNumber;
    public String ContractDate;
    public String ContractId;

    public String getText() {
        return String.format("%s|%s|%s", ContractNumber, ContractDate, ContractId);
    }
}
