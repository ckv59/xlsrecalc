/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.rosinv.xls.recalc;

import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author a
 */
public class Main {

    // Для запуска локального сервера LibreOffice выполнить в командной строке:
    // soffice -headless -accept="socket,host=127.0.0.1,port=8100;urp;" -nofirststartwizard

    public static void main(String[] args) throws Exception{
        XLSReader reader = new XLSReader("C:\\TEMP",  "", 0);
        Map<String, String> params = new HashMap<>();

        params.put("Кол-во поворотных точек ЗУ", "4"); // 3- "Неверно внесены данные (см. примечания к ячейкам)"
        params.put("зимний период", "да");
        params.put("Кол-во смежных землепользователей", "5");
        params.put("Площадь участка", "2000");
        params.put("Кол-во участков", "1");
        params.put("Протяженность границ участка (периметр)", "179");
        params.put("Количество кадастровых кварталов, в которых расположен ЗУ", "1");
        String xlsFilename = "C:/reps/rosinv/xlsrecalc/src/test/resources/ru/rosinv/xls/recalc/smeta.xls";
        byte[] xlsTemplate = Files.readAllBytes(Paths.get(xlsFilename));
        System.out.println(reader.doCalc(xlsTemplate, params));
    }
    
    public static void main1(String[] args) throws Exception {
        Map<String, String> params = new HashMap<>();

        params.put("Кол-во поворотных точек ЗУ", "15");
        params.put("зимний период", "да");

        XLSReader reader = new XLSReader("C:\\TEMP",  "", 0);
//        String xlsFilename = "C:/reps/rosinv/xlsrecalc/src/test/resources/ru/rosinv/xls/recalc/smeta.xls";
        String xlsFilename = "C:/Users/chistyakov/OneDrive/Projects/RTI/Sources/Java/xlsrecalc/src/test/resources/ru/rosinv/xls/recalc/smeta.xls";
        byte[] xlsTemplate = Files.readAllBytes(Paths.get(xlsFilename));

        ExecutorService executor = Executors.newFixedThreadPool(10);
        AtomicInteger c = new AtomicInteger();

        for (int i = 0; i < 10; i++) {
            int j = i;
            executor.execute(() -> {
                try {
                    System.out.println("start " + j);

                    byte[] rawPdf = reader.exporttoPDF(xlsTemplate, params);

                    if (rawPdf.length > 0) {
                        FileOutputStream fos = new FileOutputStream("C:/temp/temp" + j + ".pdf");

                        try {
                            fos.write(rawPdf);
                        } finally {
                            fos.close();
                        }
                    }

                    System.out.println("stop " + j);
                    c.incrementAndGet();
                } catch (Exception e) {
                    System.out.println(e.getClass().getSimpleName() + " " + e.getMessage());
                    c.incrementAndGet();
                }
            });
        }

        while (c.get() < 10) {
            Thread.sleep(500l);
        }

        reader.closeOffice();
        executor.shutdownNow();
    }

}
