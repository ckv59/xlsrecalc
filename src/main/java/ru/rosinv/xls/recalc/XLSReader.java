package ru.rosinv.xls.recalc;

import com.sun.star.beans.PropertyValue;
import com.sun.star.bridge.UnoUrlResolver;
import com.sun.star.bridge.XBridge;
import com.sun.star.bridge.XBridgeFactory;
import com.sun.star.bridge.XUnoUrlResolver;
import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.container.XIndexAccess;
import com.sun.star.container.XNamed;
import com.sun.star.frame.*;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.sheet.*;
import com.sun.star.table.*;
import com.sun.star.uno.UnoRuntime;

import static com.sun.star.uno.UnoRuntime.queryInterface;

import com.sun.star.uno.XComponentContext;
import com.sun.star.util.XCloseable;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import ru.rosinv.server.requests.price.CalcParam;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static ru.rosinv.server.tools.Tools.notEmpty;

public class XLSReader {

    private static final Logger logger = LogManager.getLogger();
    private String _tempPath;
    private String OOO_HOST;
    private int OOO_PORT;

    public XLSReader() {

    }

    public XLSReader(String tempPath, String officeHost, Integer officePort) {
        updateConfig(tempPath, officeHost, officePort.toString());
    }

    public final void updateConfig(String tempPath, String officeHost, String officePort) {
        _tempPath = tempPath;
        OOO_HOST = notEmpty(officeHost, "localhost");
        OOO_PORT = Integer.parseInt(notEmpty(officePort, "8100"));
    }

    private int getFirstParamRow(HSSFSheet sheet) {
        int result = sheet.getFirstRowNum();
        HSSFCell cell;
        HSSFRow row;

        Iterator<Row> rows = sheet.rowIterator();

        do {
            row = (HSSFRow) rows.next();
            cell = row.getCell(0);

            if (cell != null) {
                result = row.getRowNum();
                break;
            }

        } while (rows.hasNext());

        return result;
    }

    private int getLastRow(HSSFSheet sheet, int startRow, int columnIndex) {
        int result;
        HSSFCell cell;
        HSSFRow row;
        String cellValue;

        int i = startRow;

        do {
            row = sheet.getRow(i);
            if (row != null) {
                cell = row.getCell(columnIndex);
                if (cell != null) {
                    if ((cell.getCellTypeEnum() == CellType.BLANK) || (cell.getCellTypeEnum() == CellType._NONE)) {
                        cellValue = "";
                    } else
                        cellValue = "*";
                } else {
                    cellValue = "";
                }
            } else {
                cellValue = null;
            }
            i--;
        } while ((i >= 0) && ((cellValue == null) || (cellValue.length() == 0)));

        result = row.getRowNum();

        return result;
    }

    private int getLastParamRow(HSSFSheet sheet) {
        return getLastRow(sheet, sheet.getLastRowNum() - 1, 0);
    }

    private int getResultRow(HSSFSheet sheet) {
        return getLastRow(sheet, sheet.getLastRowNum(), 2);
    }

    private Object getCellValue(HSSFCell cell) {
        if ((cell.getCellTypeEnum() == CellType.NUMERIC)) {
            return new BigDecimal(cell.getNumericCellValue());
        }

        if (cell.getCellTypeEnum() == CellType.STRING) {
            return cell.getStringCellValue();
        }

        return null;
    }

    private void setParamValue(HSSFSheet sheet, String paramName, String paramValue) {
        int firstRow = getFirstParamRow(sheet);
        int lastRow = getLastParamRow(sheet);

        int i = firstRow;
        HSSFRow row;
        HSSFCell cell;

        String cellValue;
        boolean foundParam;
        String unit;

        do {
            row = sheet.getRow(i);
            cell = row.getCell(0);

            if (cell != null) {
                cellValue = (String) getCellValue(cell);
            } else {
                cellValue = null;
            }

            foundParam = Objects.equals(cellValue, paramName);

            i++;
        } while ((i <= lastRow) && (!foundParam));

        if (foundParam) {
            cell = row.getCell(1);
            unit = (String) getCellValue(cell);
            unit = unit == null ? "" : unit.toLowerCase();

            cell = row.getCell(2);

            switch (unit) {
                case "кв.м.":
                case "кв.м":
                case "м":
                case "м.":
                case "у.е.":
                case "га.":
                    cell.setCellValue(Double.parseDouble(paramValue));
                    break;
                case "номер":
                case "шт.":
                case "%":
                    cell.setCellValue(Integer.parseInt(paramValue));
                    break;
                default:
                    cell.setCellValue(paramValue);
                    break;
            }
        }
    }

    private HSSFWorkbook recalcWorkBook(byte[] xlsTemplate, Map<String, String> params) throws Exception {
        InputStream ExcelFileToRead = new ByteArrayInputStream(xlsTemplate);
        HSSFWorkbook wb;

        wb = new HSSFWorkbook(ExcelFileToRead);
        HSSFSheet sheet = wb.getSheetAt(0);

        int firstRow = getFirstParamRow(sheet);
        int lastRow = getLastParamRow(sheet);
        int resultRow = getResultRow(sheet);

        if ((firstRow == 0) && (lastRow == 0) && (resultRow == 0)) {
            logger.log(Level.ERROR, "bad template body");
            throw new Exception("bad template body");
        }

        params.forEach((paramName, paramValue) -> setParamValue(sheet, paramName, paramValue));

        return wb;
    }

    private String getStringCellValue(HSSFCell cell) {
        if (cell == null) {
            return null;
        }
        CellType cellType = cell.getCellTypeEnum();
        switch (cellType) {
            case NUMERIC:
                return "" + cell.getNumericCellValue();
            default:
                return cell.getStringCellValue();
        }
    }

    public List<CalcParam> readParamUnits(byte[] xlsTemplate) {
        try {
            List<CalcParam> result = new ArrayList<>();
            InputStream ExcelFileToRead = new ByteArrayInputStream(xlsTemplate);
            try (HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead)) {
                HSSFSheet sheet = wb.getSheet("исходные");
                if (sheet == null) {
                    return result;
                }
                HSSFRow row;
                HSSFCell cell;

                int firstRow = getFirstParamRow(sheet);
                int lastRow = getLastParamRow(sheet);
                String paramName, unitName;
//                Object paramValue;

                for (int i = firstRow; i <= lastRow; i++) {
                    row = sheet.getRow(i);

                    cell = row.getCell(0);
                    paramName = cell.getStringCellValue();
                    if (paramName != null && !paramName.isEmpty()) {
                        cell = row.getCell(1);
                        unitName = getStringCellValue(cell);
                        String min = getStringCellValue(row.getCell(3));
                        String max = getStringCellValue(row.getCell(4));

//                    cell = (HSSFCell) row.getCell(2);
//                    paramValue = getCellValue(cell);
                        result.add(new CalcParam(UUID.randomUUID().toString(), paramName, unitName, min, max));
                    }
                }

                return result;
            }
        } catch (Exception e) {
            logger.error("bad template", e);
            return new ArrayList<CalcParam>();
        }
    }

    public String doCalc(byte[] xlsTemplate, Map<String, String> params) throws Exception {

        HSSFWorkbook wb = recalcWorkBook(xlsTemplate, params);

        Path sXlsFileName = Paths.get(_tempPath, UUID.randomUUID().toString() + ".xls");

        try (FileOutputStream fos = new FileOutputStream(sXlsFileName.toString())) {
            wb.write(fos);
        }

        XComponentContext xContext = Bootstrap.createInitialComponentContext(null);
        // create a connector, so that it can contact the office
        XUnoUrlResolver urlResolver = UnoUrlResolver.create(xContext);

        Object initialObject = urlResolver.resolve(
                String.format("uno:socket,host=%s,port=%d;urp;StarOffice.ServiceManager", OOO_HOST, OOO_PORT));

        XMultiComponentFactory xOfficeFactory = UnoRuntime.queryInterface(
                XMultiComponentFactory.class, initialObject);

        Object oDesktop = xOfficeFactory.createInstanceWithContext("com.sun.star.frame.Desktop", xContext);

        XComponentLoader xCompLoader = UnoRuntime.queryInterface(XComponentLoader.class, oDesktop);

        String sUrl = ("file:///" + sXlsFileName).replace('\\', '/');

        FileOutputStream fos = new FileOutputStream(sXlsFileName.toString());
        try {
            wb.write(fos);
        } finally {
            fos.close();
        }

        HSSFSheet sheet = wb.getSheetAt(0);
        int resultRow = getResultRow(sheet);

        PropertyValue propertyValues[] = new PropertyValue[1];
        propertyValues[0] = new PropertyValue();
        propertyValues[0].Name = "Hidden";
        propertyValues[0].Value = Boolean.TRUE;

        Object xSpreadsheetComponent = xCompLoader.loadComponentFromURL(sUrl, "_blank", 0, propertyValues);
        XSpreadsheetDocument xSpreadsheetDocument = UnoRuntime.queryInterface(XSpreadsheetDocument.class, xSpreadsheetComponent);
        Object sheets = xSpreadsheetDocument.getSheets();
        XIndexAccess xIndexedSheets = UnoRuntime.queryInterface(
                XIndexAccess.class, sheets);
        Object xSheet = xIndexedSheets.getByIndex(0);
        XSpreadsheet xSpreadsheet = UnoRuntime.queryInterface(XSpreadsheet.class, xSheet);
        XCell xCell = xSpreadsheet.getCellByPosition(2, resultRow);

        double value = xCell.getValue();

        XCloseable xCloseable = UnoRuntime.queryInterface(XCloseable.class, xSpreadsheetComponent);

        if (xCloseable != null) {
            xCloseable.close(false);
        } else {
            XComponent xComp = UnoRuntime.queryInterface(XComponent.class, xSpreadsheetComponent);

            xComp.dispose();
        }

        Files.delete(sXlsFileName);

        return new BigDecimal(value).setScale(2, RoundingMode.HALF_UP).toPlainString();
    }

    public byte[] exporttoPDF(byte[] xlsTemplate, Map<String, String> params) throws Exception {

        HSSFWorkbook wb = recalcWorkBook(xlsTemplate, params);

        XComponentContext xContext = Bootstrap.createInitialComponentContext(null);
        // create a connector, so that it can contact the office
        XUnoUrlResolver urlResolver = UnoUrlResolver.create(xContext);

        Object initialObject = urlResolver.resolve(
                String.format("uno:socket,host=%s,port=%d;urp;StarOffice.ServiceManager", OOO_HOST, OOO_PORT));

        XMultiComponentFactory xOfficeFactory = UnoRuntime.queryInterface(
                XMultiComponentFactory.class, initialObject);

        Object oDesktop = xOfficeFactory.createInstanceWithContext("com.sun.star.frame.Desktop", xContext);

        XComponentLoader xCompLoader = UnoRuntime.queryInterface(XComponentLoader.class, oDesktop);

        Path sXlsFileName = Paths.get(_tempPath, UUID.randomUUID().toString() + ".xls");
        String sUrl = ("file:///" + sXlsFileName).replace('\\', '/');
        String sStoreUrl = sUrl.replace(".xls", ".pdf");

        FileOutputStream fos = new FileOutputStream(sXlsFileName.toString());
        try {
            wb.write(fos);
        } finally {
            fos.close();
        }

        PropertyValue propertyValues[] = new PropertyValue[1];
        propertyValues[0] = new PropertyValue();
        propertyValues[0].Name = "Hidden";
        propertyValues[0].Value = Boolean.TRUE;

        Object oDocToStore = xCompLoader.loadComponentFromURL(sUrl, "_blank", 0, propertyValues);
        XSpreadsheetDocument xSpreadsheetDocument = UnoRuntime.queryInterface(XSpreadsheetDocument.class, oDocToStore);
        XSpreadsheets xSpreadsheets = xSpreadsheetDocument.getSheets();
        Object sheet = xSpreadsheets.getByName("СМЕТА");
        XStorable xStorable = UnoRuntime.queryInterface(XStorable.class, oDocToStore);

        // Preparing properties for converting the document
        PropertyValue[] aFilterData = new PropertyValue[1];
        aFilterData[0] = new PropertyValue();
        aFilterData[0].Name = "Selection";
        aFilterData[0].Value = sheet;

        propertyValues = new PropertyValue[3];
        // Setting the flag for overwriting
        propertyValues[0] = new PropertyValue();
        propertyValues[0].Name = "Overwrite";
        propertyValues[0].Value = Boolean.TRUE;
        // Setting the filter name
        propertyValues[1] = new PropertyValue();
        propertyValues[1].Name = "FilterName";
        propertyValues[1].Value = "calc_pdf_Export";
        // Setting options for PDF Export
        propertyValues[2] = new PropertyValue();
        propertyValues[2].Name = "FilterData";
        propertyValues[2].Value = aFilterData;

        // Storing and converting the document
        xStorable.storeToURL(sStoreUrl, propertyValues);

        // Closing the converted document. Use XCloseable.close if the
        // interface is supported, otherwise use XComponent.dispose
        XCloseable xCloseable = UnoRuntime.queryInterface(XCloseable.class, xStorable);

        if (xCloseable != null) {
            xCloseable.close(false);
        } else {
            XComponent xComp = UnoRuntime.queryInterface(XComponent.class, xStorable);

            xComp.dispose();
        }

        // ((XComponent) oDocToStore).dispose();
        Path pdfPath = Paths.get(new URL(sStoreUrl).toURI());
        byte[] result = Files.readAllBytes(pdfPath);

        Files.delete(pdfPath);
        Files.delete(sXlsFileName);

        return result;
    }

    public byte[] exporttoXLS(byte[] xlsTemplate, Map<String, String> params) throws Exception {

        HSSFWorkbook wb = recalcWorkBook(xlsTemplate, params);

        XComponentContext xContext = Bootstrap.createInitialComponentContext(null);
        // create a connector, so that it can contact the office
        XUnoUrlResolver urlResolver = UnoUrlResolver.create(xContext);

        Object initialObject = urlResolver.resolve(
                String.format("uno:socket,host=%s,port=%d;urp;StarOffice.ServiceManager", OOO_HOST, OOO_PORT));

        XMultiComponentFactory xOfficeFactory = UnoRuntime.queryInterface(
                XMultiComponentFactory.class, initialObject);

        Object oDesktop = xOfficeFactory.createInstanceWithContext("com.sun.star.frame.Desktop", xContext);

        XComponentLoader xCompLoader = UnoRuntime.queryInterface(XComponentLoader.class, oDesktop);

        Path sourceFilenamePath = Paths.get(_tempPath, UUID.randomUUID().toString() + ".xls");
        String sourceFilenameUrl = ("file:///" + sourceFilenamePath).replace('\\', '/');

        FileOutputStream fos = new FileOutputStream(sourceFilenamePath.toString());
        try {
            wb.write(fos);
        } finally {
            fos.close();
        }

        PropertyValue propertyValues[] = new PropertyValue[1];
        propertyValues[0] = new PropertyValue();
        propertyValues[0].Name = "Hidden";
        propertyValues[0].Value = Boolean.TRUE;

        Object xSourceSpreadsheetComponent = xCompLoader.loadComponentFromURL(sourceFilenameUrl, "_blank", 0, propertyValues);
        XSpreadsheetDocument xSourceSpreadsheetDocument = UnoRuntime.queryInterface(XSpreadsheetDocument.class, xSourceSpreadsheetComponent);
        XSpreadsheets xSourceSpreadsheets = xSourceSpreadsheetDocument.getSheets();
        Object objSourceSheet = xSourceSpreadsheets.getByName("СМЕТА");
        XSpreadsheet sourceSheet = UnoRuntime.queryInterface(XSpreadsheet.class, objSourceSheet);
        XUsedAreaCursor used = queryInterface(XUsedAreaCursor.class, sourceSheet.createCursor());
        used.gotoStartOfUsedArea(false);
        used.gotoEndOfUsedArea(true);
        XCellRange usedRange = queryInterface(XCellRange.class, used);
        XCellRangeData rangeData = queryInterface(XCellRangeData.class, usedRange);
        Object[][] data = rangeData.getDataArray();
        queryInterface(XSheetOperation.class, usedRange).clearContents(CellFlags.FORMULA);
        rangeData.setDataArray(data);

        XIndexAccess sheetsIA = queryInterface(XIndexAccess.class, xSourceSpreadsheets);
        for (int i = sheetsIA.getCount() - 1; i >= 0; i--) {
            XNamed named = queryInterface(XNamed.class, sheetsIA.getByIndex(i));
            if (!named.getName().equals("СМЕТА")) {
                xSourceSpreadsheets.removeByName(named.getName());
            }
        }

        XCellRange printRange = queryInterface(XCellRange.class, sourceSheet).getCellRangeByName("Excel_BuiltIn_Print_Area");

        XCellRangeAddressable usedColRange = queryInterface(XCellRangeAddressable.class, usedRange);
        XCellRangeAddressable printColRange = queryInterface(XCellRangeAddressable.class, printRange);

        XColumnRowRange usedRowsCols = queryInterface(XColumnRowRange.class, usedColRange);
        XTableColumns usedColumns = usedRowsCols.getColumns();

        for (int i = printColRange.getRangeAddress().EndColumn + 1; i <= usedColRange.getRangeAddress().EndColumn; i++) {
            queryInterface(XSheetOperation.class, usedColumns.getByIndex(i)).clearContents(
                    CellFlags.HARDATTR + CellFlags.STRING + CellFlags.DATETIME + +CellFlags.VALUE);
        }

        // Storing the document
        XStorable xStorable = queryInterface(XStorable.class, xSourceSpreadsheetComponent);
        xStorable.store();

        // Closing the converted document. Use XCloseable.close if the
        // interface is supported, otherwise use XComponent.dispose
        XCloseable xCloseable = UnoRuntime.queryInterface(XCloseable.class, xStorable);

        if (xCloseable != null) {
            xCloseable.close(false);
        } else {
            XComponent xComp = UnoRuntime.queryInterface(XComponent.class, xStorable);

            xComp.dispose();
        }

        xCloseable = UnoRuntime.queryInterface(XCloseable.class, xSourceSpreadsheetComponent);
        if (xCloseable != null) {
            xCloseable.close(false);
        }

        byte[] result = Files.readAllBytes(sourceFilenamePath);

        Files.delete(sourceFilenamePath);

        return result;
    }

    void closeOffice() {
        try {
            // get the bridge factory from the local service manager
            XBridgeFactory bridgeFactory = UnoRuntime.queryInterface(XBridgeFactory.class,
                    Bootstrap.createSimpleServiceManager()
                            .createInstance("com.sun.star.bridge.BridgeFactory"));

            if (bridgeFactory != null) {
                for (XBridge bridge : bridgeFactory.getExistingBridges()) {
                    // dispose of this bridge after closing its connection
                    UnoRuntime.queryInterface(XComponent.class, bridge).dispose();
                }
            }
        } catch (Throwable e) {
            logger.error("Exception disposing office process connection bridge:", e);
        }
    }
}
