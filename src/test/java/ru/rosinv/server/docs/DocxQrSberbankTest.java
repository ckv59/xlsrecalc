package ru.rosinv.server.docs;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

class DocxQrSberbankTest {
    private static byte[] docxTemplate;
    private static final String docxFilename = "blank.docx";

    @BeforeAll
    static void initTest() throws Exception {
        URL filename = DocxQrSberbankTest.class.getResource(docxFilename);
        docxTemplate = Files.readAllBytes(Paths.get(filename.toURI()));
    }

    @Test
    void generateQrSberbankTest() throws IOException {

        final String filename = "C:/temp/qrcode_sber.docx";

        if (Files.exists(Paths.get(filename), LinkOption.NOFOLLOW_LINKS)) {
            Files.delete(Paths.get(filename));
        }

        final Map<String, String> params = new HashMap<>();

        params.put("${QrSberbank_Name}", "Приуральский филиал АО \"Ростехинвентаризация - Федеральное БТИ\" в Пермском отделении  N 6984/0282 г. Пермь");
        params.put("${QrSberbank_PersonalAcc}", "40502810049490050051");
        params.put("${QrSberbank_BankName}", "ВОЛГО-ВЯТСКИЙ БАНК ПАО СБЕРБАНК");
        params.put("${QrSberbank_BIC}", "042202603");
        params.put("${QrSberbank_CorrespAcc}", "30101810900000000603");
        params.put("${QrSberbank_PayeeINN}", "9729030514");
        params.put("${QrSberbank_LastName}", "Рамзин");
        params.put("${QrSberbank_FirstName}", "Анатолий");
        params.put("${QrSberbank_MiddleName}", "Александрович");
        params.put("${QrSberbank_PayerAddress}", "Россия, Пермский край, с.Берёзовка, ул.Ленина, д.50");
        params.put("${QrSberbank_Purpose}", "№ договора: 18-5911-Д/270 от 31.07.2018");
        params.put("${QrSberbank_Sum}", "125400");
        params.put("${QrSberbank_paymPeriod}", "072018");

        params.put("${QrSpecCode_Number}", "1817-5904-Д/103");
        params.put("${QrSpecCode_Date}", "2018-07-16");
        params.put("${QrSpecCode_Id}", "8a2859b5-7eb1-45f2-b598-26e80b2dea4d");

        byte[] newDocxTemplate = Docx.replaceParams(docxTemplate, params);

        try (FileOutputStream fos = new FileOutputStream(filename)) {
            fos.write(newDocxTemplate);
        }

        assertTrue(Files.exists(Paths.get(filename), LinkOption.NOFOLLOW_LINKS));
    }
}
