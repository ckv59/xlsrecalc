package ru.rosinv.server.tools;

import com.google.zxing.WriterException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.rosinv.server.tools.qrcode.QrCodeGenerator;
import ru.rosinv.server.tools.qrcode.QrSberbankCodeRequisites;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertTrue;

class QrCodeGeneratorTest {
    private static QrSberbankCodeRequisites requisites;

    @BeforeAll
    static void initRequisites()
    {
        requisites = new QrSberbankCodeRequisites();

        requisites.Name = "Приуральский филиал АО \"Ростехинвентаризация - Федеральное БТИ\" в Пермском отделении  N 6984/0282 г. Пермь";
        requisites.PersonalAcc = "40502810049490050051";
        requisites.BankName = "ВОЛГО-ВЯТСКИЙ БАНК ПАО СБЕРБАНК";
        requisites.BIC = "042202603";
        requisites.CorrespAcc = "30101810900000000603";
        requisites.LastName = "Рамзин";
        requisites.FirstName = "Анатолий";
        requisites.MiddleName = "Александрович";
        requisites.PayeeINN = "9729030514";
        requisites.PayerAddress = "Россия, Пермский край, с.Берёзовка, ул.Ленина, д.50";
        requisites.Purpose = "№ договора: 18-5911-Д/270 от 31.07.2018";
        requisites.Sum = "131700";
        requisites.paymPeriod = "072018";
    }

    @Test
    void generateQrCodeImage() throws IOException, WriterException {
        QrCodeGenerator qrCodeGenerator = new QrCodeGenerator();

        byte[] qrCodeImageData = qrCodeGenerator.getQRCodeImageData(requisites.getText(), 300, 300);

        String filename = "C:/temp/qrcode.png";

        Files.deleteIfExists(Paths.get(filename));

        try (FileOutputStream fos = new FileOutputStream(filename)) {
            fos.write(qrCodeImageData);
        }

        assertTrue(Files.exists(Paths.get(filename), LinkOption.NOFOLLOW_LINKS));
    }
}
