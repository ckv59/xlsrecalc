package ru.rosinv.xls.recalc;

import org.junit.jupiter.api.Test;

import ru.rosinv.server.contracts.Contract;
import ru.rosinv.server.contracts.ContractsXls;
import ru.rosinv.server.security.File;
import ru.rosinv.server.tools.Json;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ContractsExportTest {

    @Test
    void TestExportDoers() throws URISyntaxException, IOException {
        Json json = new Json();

        List<Contract> contractList = new ArrayList<Contract>();

        URL filename = XLSReaderTest.class.getResource("contract1.json");
        String contents = new String(Files.readAllBytes(Paths.get(filename.toURI())));
        Contract c = json.read(contents, Contract.class);
        contractList.add(c);

        filename = XLSReaderTest.class.getResource("contract2.json");
        contents = new String(Files.readAllBytes(Paths.get(filename.toURI())));
        c = json.read(contents, Contract.class);
        contractList.add(c);

        ContractsXls xls = new ContractsXls();
        File f = xls.createXls(contractList, "", "", "", "", "", "", "", "", "", "", "", "", "", "");
        byte[] rawXls = f.getBytes();

        FileOutputStream fos = new FileOutputStream("C:/temp/contracts.xls");
        try {
            fos.write(rawXls);
        } finally {
            fos.close();
        }

        assertTrue(Files.exists(Paths.get("C:/temp/contracts.xls")));
    }
}
