package ru.rosinv.xls.recalc;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

import ru.rosinv.server.requests.price.CalcParam;

class XLSReaderTest {

    private static byte[] xlsTemplate;
    private static final String xlsFilename = "smeta-1810.xls";
    private static final Map<String, String> params = new HashMap<>();

    @BeforeAll
    static void init() throws Exception {
        URL filename = XLSReaderTest.class.getResource(xlsFilename);
        xlsTemplate = Files.readAllBytes(Paths.get(filename.toURI()));

        params.put("Протяженность объекта", "150");
        params.put("Ширина охраняемой зоны", "20");
        params.put("Кол-во участков", "3");
        params.put("Количество кадастровых кварталов", "1");
    }

    @Test
    void readParamUnitsTest() {
        final List<CalcParam> paramUnits;

        XLSReader reader = new XLSReader("C:/TEMP", "", 0);
        paramUnits = reader.readParamUnits(xlsTemplate);

        if (!paramUnits.isEmpty()) {
            paramUnits.forEach(param -> {
                System.out.printf("%s: %s", param.getName(), param.getValue());
                System.out.println();
            });
        }

        assertFalse(paramUnits.isEmpty());
    }

    @Test
    void doCalcTest() throws Exception {
        XLSReader reader = new XLSReader("C:/TEMP", "localhost", 8100);
        String sum = null;

        sum = reader.doCalc(xlsTemplate, params);

        System.out.printf("Sum: %s", sum);

        assertFalse(sum == null);
    }

    @Test
    void exporttoPDFTest() throws Exception {
        // Run LibreOffice: soffice --headless --accept="socket,host=127.0.0.1,port=8100;urp;" --nofirststartwizard

        XLSReader reader = new XLSReader("C:\\TEMP", "localhost", 8100);

        byte[] rawPdf = reader.exporttoPDF(xlsTemplate, params);

        FileOutputStream fos = new FileOutputStream("C:/temp/temp.pdf");
        try {
            fos.write(rawPdf);
        } finally {
            fos.close();
        }
    }

    @Test
    void exporttoXLSTest() throws Exception {
        // Run LibreOffice: soffice --headless --accept="socket,host=127.0.0.1,port=8100;urp;" --nofirststartwizard

        XLSReader reader = new XLSReader("C:\\TEMP", "localhost", 8100);

        byte[] rawXls = reader.exporttoXLS(xlsTemplate, params);

        FileOutputStream fos = new FileOutputStream("C:/temp/temp.xls");
        try {
            fos.write(rawXls);
        } finally {
            fos.close();
        }
    }
    
	public static void main(String[] args) throws Exception {
        // Run LibreOffice in Windows: soffice -headless -accept="socket,host=127.0.0.1,port=8100;urp;" -nofirststartwizard
		// дока там https://www.openoffice.org/api/docs/common/ref/com/sun/star/sheet/XPrintAreas.html
		init();
		XLSReaderTest test = new XLSReaderTest();
		test.exporttoXLSTest();
		System.out.println("OK");
	}
}
